from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail
import os
from flask_login import LoginManager, AnonymousUserMixin


APP_ROOT = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLDER = os.path.join(APP_ROOT, 'static/photos/uploads')
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'JPG', 'PNG', 'JPEG'])

app = Flask(__name__)
app.config.from_object('config')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


class AnonymousUser(AnonymousUserMixin):
    @property
    def date_format(self):
        return 'YYYY-MM-DD'

    @property
    def has_premium_features(self):
        return False


login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "api.Login"
login_manager.anonymous_user = AnonymousUser

db = SQLAlchemy(app)

mail = Mail()
mail.init_app(app)


from flask import render_template, request, redirect
from web.api.routes import api
from web.api.models.log import Log
from web.api.models.user import User
from web import db
import datetime
from flask_login import current_user


@login_manager.user_loader
def load_user(user_id):
    return db.session.query(User).filter(User.id == user_id).first()


app.register_blueprint(api, url_prefix='/')


# Обработка ошибок
TIME = datetime.datetime.now().strftime("%H:%M %d.%m.%Y")
@app.errorhandler(500)
def internal_serve_error(error):
    log = Log('500', request.path, TIME, current_user.login)
    log.save()
    return render_template('errors/500.html')


@app.errorhandler(404)
def not_found(error):
    log = Log('404', request.path, TIME, current_user.login)
    log.save()
    return render_template('errors/404.html')


@app.errorhandler(413)
def too_big_file(error):
    log = Log('413', request.path, TIME, current_user.login)
    log.save()
    return render_template('errors/413.html')


@app.errorhandler(400)
def bad_request(error):
    log = Log('400', request.path, TIME, current_user.login)
    log.save()
    return render_template('errors/400.html')


@app.errorhandler(503)
def unavailable(error):
    log = Log('503', request.path, TIME, current_user.login)
    log.save()
    return render_template('errors/503.html')


@app.errorhandler(504)
def timeout(error):
    log = Log('504', request.path, TIME, current_user.login)
    log.save()
    return render_template('errors/504.html')


@app.route('/error/rights', methods=['GET'])
def error_rights():
    return redirect('/error/right')
