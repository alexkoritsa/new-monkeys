from web import db
from web.api.models.team import Team
from flask_login import UserMixin
from web.api.models.tournament import Tournament


class FinishQuest(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    id_team = db.Column(db.Integer)
    id_quest = db.Column(db.Integer)
    score = db.Column(db.Integer)

    def __init__(self, id_team, id_quest, score):
        self.id_team = id_team
        self.id_quest = id_quest
        self.score = score

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id

    @staticmethod
    def get_all():
        return FinishQuest.query.all()

    @staticmethod
    def get_score_team(id_team):
        team = db.session.query(Team).filter(Team.id == id_team).first()
        final_score = 0
        finishes = FinishQuest.get_all()
        for f in finishes:
            if f.id_team == team.id:
                final_score += f.score
        return final_score

    @staticmethod
    def clean_table_in_finish_tournament():
        if Tournament.is_active_tournament():
            FinishQuest.query.delete()
            return True
        else:
            return False
