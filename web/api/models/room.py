from web import db
from web.api.models.answer import Answer
from flask_login import UserMixin


class Room(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(256))
    question = db.Column(db.String(1024))
    picture = db.Column(db.String(256), nullable=True)
    answers = db.relationship('Answer', backref='room', lazy='dynamic')
    quest_id = db.Column(db.Integer, db.ForeignKey('quest.id'))
    creator = db.Column(db.Integer)
    code = db.Column(db.String(256))
    is_finish = db.Column(db.String(64))
    thing_to_user = db.Column(db.String(256))
    thing_to_enter = db.Column(db.String(256))

    def __init__(self, title, question, picture, quest_id, creator, code, is_finish, thing_to_user, thing_to_enter):
        self.title = title
        self.question = question
        self.picture = picture
        self.quest_id = quest_id
        self.creator = creator
        self.code = code
        self.is_finish = is_finish
        self.thing_to_user = thing_to_user
        self.thing_to_enter = thing_to_enter

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id

    def remove(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_all():
        return Room.query.all()

    def add_answers(self, answers):
        if len(answers) == 0:
            self.remove()
            return False
        if len(answers) == 1:
            answer_incorrect = Answer('Неправильный ответ', self.id)
            answer_incorrect.save()
        for ans in answers:
            answer = Answer(ans, self.id)
            answer.save()
        return True

    @staticmethod
    def get_how_many_rooms_in_quest(quest_id):
        return len(db.session.query(Room).filter(Room.quest_id == quest_id).all())

    @staticmethod
    def update_by_id(id, key, value):
        db.session.query(Room).filter(Room.id == id).update({key: value}, synchronize_session='evaluate')
        db.session.commit()

    @staticmethod
    def get_all_secret_codes():
        return db.session.query(Room.code).all()

    @staticmethod
    def check_photo_file_name(filename):
        if db.session.query(Room).filter(Room.picture == filename).first():
            return False
        return True
