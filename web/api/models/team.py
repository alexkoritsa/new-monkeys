from web import db
from flask_login import UserMixin


class Team(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256))
    region = db.Column(db.String(256))
    password = db.Column(db.String(256))
    creator = db.Column(db.String(256))
    photo = db.Column(db.String(256))

    def __init__(self, name, region, password, creator, photo='0'):
        self.name = name
        self.region = region
        self.password = password
        self.creator = creator
        self.photo = photo

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id

    @staticmethod
    def get_all():
        return Team.query.all()

    @staticmethod
    def update_by_id(id, key, value):
        db.session.query(Team).filter(Team.id == id).update({key: value}, synchronize_session='evaluate')
        db.session.commit()
