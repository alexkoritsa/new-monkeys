from web import db
from flask_login import UserMixin


class Tournament(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    status = db.Column(db.String(256))
    start = db.Column(db.String(256))
    teams = db.Column(db.String(2048))
    end = db.Column(db.String(256))
    winner = db.Column(db.String(2048))
    best_quest = db.Column(db.String(2048))

    def __init__(self, status, start, teams, end='', winner='', best_quest=''):
        self.status = status
        self.start = start
        self.teams = teams
        self.end = end
        self.winner = winner
        self.best_quest = best_quest

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id

    @staticmethod
    def update_by_id(id, key, value):
        db.session.query(Tournament).filter(Tournament.id == id).update({key: value}, synchronize_session='evaluate')
        db.session.commit()

    @staticmethod
    def is_active_tournament():
        if db.session.query(Tournament).filter(Tournament.status == 'ACTIVE').first():
            return True
        else:
            return False
