from web import db
from flask_login import UserMixin


class UserToTeam(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    id_user = db.Column(db.Integer)
    id_team = db.Column(db.Integer)
    now = db.Column(db.Boolean)

    # Эта модель нужна для отражения всех команд, в которых был пользователь.
    # Поле now как раз отвечает - пользователь сейчас в этой команде или нет

    def __init__(self, id_user, id_team, now):
        self.id_user = id_user
        self.id_team = id_team
        self.now = now

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id

    @staticmethod
    def get_all():
        return UserToTeam.query.all()
