from web import db
from flask_login import UserMixin


class Attempt(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    id_quest = db.Column(db.Integer)
    id_room = db.Column(db.Integer)
    team = db.Column(db.String)
    type = db.Column(db.String(128))
    content = db.Column(db.String(2048))
    time = db.Column(db.String(256))
    success = db.Column(db.Boolean)

    def __init__(self, id_quest, id_room, team, type, content, time, success):
        self.id_quest = id_quest
        self.id_room = id_room
        self.team = team
        self.type = type
        self.content = content
        self.time = time
        self.success = success

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id

    @staticmethod
    def get_all():
        return Attempt.query.all()

    @staticmethod
    def get_all_attempt_from_team_name(team_name):
        att = db.session.query(Attempt).filter(Attempt.team == team_name).all()
        return att[::-1]

    @staticmethod
    def get_all_attempt_in_room(id_room):
        att = db.session.query(Attempt).filter(Attempt.id_room == id_room).all()
        return att[::-1]
