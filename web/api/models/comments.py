from flask_login import UserMixin
from web import db


class Comment(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(256))
    author = db.Column(db.String(256))
    time = db.Column(db.String(256))
    post_id = db.Column(db.Integer)

    def __init__(self, content, author, time, post_id):
        self.content = content
        self.author = author
        self.time = time
        self.post_id = post_id

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id

    @staticmethod
    def get_all():
        return Comment.query.all()

    @staticmethod
    def get_comment_on_post(id):
        comments = Comment.get_all()
        comm = list()
        for i in comments:
            if i.post_id == id:
                comm.append(i)
        return comm

    @staticmethod
    def update_by_id(id, key, value):
        db.session.query(Comment).filter(Comment.id == id).update({key: value}, synchronize_session='evaluate')
        db.session.commit()
