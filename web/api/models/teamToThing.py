from web import db
from flask_login import UserMixin


class TeamToThing(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    team_id = db.Column(db.Integer)
    quest_id = db.Column(db.Integer)
    thing = db.Column(db.String(256))

    def __init__(self, team_id, quest_id, thing):
        self.team_id = team_id
        self.quest_id = quest_id
        self.thing = thing

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id

    @staticmethod
    def get_all():
        return TeamToThing.query.all()

    @staticmethod
    def check_if_team_have_thing(team_id, quest_id, thing):
        if db.session.query(TeamToThing).filter(TeamToThing.team_id == team_id).filter(TeamToThing.quest_id == quest_id).filter(TeamToThing.thing == thing).first():
            return True
        else:
            return False
