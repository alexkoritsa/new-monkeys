from web import db
from sqlalchemy import or_
from flask_login import UserMixin


class Log(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    error = db.Column(db.String(256))
    page = db.Column(db.String(128))
    time = db.Column(db.String(256))
    user = db.Column(db.String(256))

    def __init__(self, error, page, time, user):
        self.error = error
        self.page = page
        self.time = time
        self.user = user

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id

    @staticmethod
    def get_all():
        return Log.query.all()

    @staticmethod
    def get_404_errors():
        errors = db.session.query(Log).filter(Log.error == '404').all()
        return errors[::-1]

    @staticmethod
    def get_404_errors_not_reverse():
        return db.session.query(Log).filter(Log.error == '404').all()

    @staticmethod
    def get_users_errors():
        errors = db.session.query(Log).filter(Log.error.like('%WRONG PASS%')).all()
        return errors[::-1]

    @staticmethod
    def get_server_errors():
        errors = db.session.query(Log).filter(or_(Log.error == '503', Log.error == '504', Log.error == '400',
                                                  Log.error == '413')).all()
        return errors[::-1]

    @staticmethod
    def get_server_errors_not_reverse():
        return db.session.query(Log).filter(or_(Log.error == '503', Log.error == '504', Log.error == '400',
                                                  Log.error == '413')).all()

    @staticmethod
    def get_users_errors_not_reverse():
        return db.session.query(Log).filter(Log.error.like('%WRONG PASS%')).all()
