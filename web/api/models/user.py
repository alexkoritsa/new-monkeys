from web import db
from hashlib import sha256
from flask_login import UserMixin


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256))
    login = db.Column(db.String(256))
    email = db.Column(db.String(256))
    password = db.Column(db.String(256))
    code = db.Column(db.String(256))
    role = db.Column(db.String(256))
    mails = db.Column(db.Integer)
    team_id = db.Column(db.Integer)
    photo = db.Column(db.String(256))
    active_tournament = db.Column(db.String(256))

    def __init__(self, name, login, email, password, code, role, mails=0, team_id=0, photo='0', active_tournament='INACTIVE'):
        self.name = name
        self.login = login
        self.email = email
        self.password = password
        self.code = code
        self.role = role
        self.mails = mails
        self.team_id = team_id
        self.photo = photo
        self.active_tournament = active_tournament

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id

    @staticmethod
    def get_all():
        return User.query.all()

    @staticmethod
    def get_user_by_id(user_id):
        return db.session.query(User).filter(User.id == user_id).first()

    @staticmethod
    def get_password_from_login(login):
        return db.session.query(User.password).filter(User.login == login)

    @staticmethod
    def update_by_login(login, key, value):
        db.session.query(User).filter(User.login == login).update({key: value}, synchronize_session='evaluate')
        db.session.commit()

    @staticmethod
    def change_password_by_email(email, password):
        secret_password = sha256(password.encode()).hexdigest()
        User.query.filter(User.email == email).update(dict(password=secret_password))
        db.session.commit()

    @staticmethod
    def last_id():
        return db.session.query(User.id).order_by(User.id.desc()).first()

    @staticmethod
    def get_how_much_players_in_one_team(id):
        players = db.session.query(User.id).filter(User.team_id == id).all()
        return len(players)

    @staticmethod
    def make_all_users_active():
        User.query.update(dict(active_tournament='ACTIVE'))
        db.session.commit()

    @staticmethod
    def make_all_users_inactive():
        User.query.update(dict(active_tournament='INACTIVE'))
        db.session.commit()

    @staticmethod
    def check_photo_file_name(filename):
        if db.session.query(User).filter(User.photo == filename).first():
            return False
        return True
