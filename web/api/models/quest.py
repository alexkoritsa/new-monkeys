from web import db
from web.api.models.room import Room
from flask_login import UserMixin


class Quest(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256))
    rating = db.Column(db.Integer)
    creator_team_id = db.Column(db.Integer)
    region_creator_team = db.Column(db.String(256))
    rating_from_leader = db.Column(db.Integer)
    rooms = db.relationship('Room', backref='quest', lazy='dynamic')

    def __init__(self, name, rating, creator_team_id, region_creator_team, rating_from_leader=0):
        self.name = name
        self.rating = rating
        self.creator_team_id = creator_team_id
        self.region_creator_team = region_creator_team
        self.rating_from_leader = rating_from_leader

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id

    @staticmethod
    def get_all():
        return Quest.query.all()

    @staticmethod
    def get_all_rooms_in_quest(quest_id):
        return db.session.query(Room).filter(Room.quest_id == quest_id).all()

    @staticmethod
    def get_how_many_quest_from_one_team(team_id):
        return len(db.session.query(Quest).filter(Quest.creator_team_id == team_id).all())

    @staticmethod
    def update_by_id(id, key, value):
        db.session.query(Quest).filter(Quest.id == id).update({key: value}, synchronize_session='evaluate')
        db.session.commit()
