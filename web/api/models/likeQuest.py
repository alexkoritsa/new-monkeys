from web import db
from flask_login import UserMixin
from web.api.models.tournament import Tournament


class LikeQuest(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    id_team = db.Column(db.Integer)
    id_quest = db.Column(db.Integer)
    cool = db.Column(db.Boolean)  # Поле отвечает за то, что это будет: лайк или дизлайк

    def __init__(self, id_team, id_quest, cool):
        self.id_team = id_team
        self.id_quest = id_quest
        self.cool = cool

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id

    @staticmethod
    def get_all():
        return LikeQuest.query.all()

    @staticmethod
    def get_all_likes_to_quest(id_quest):
        likes = db.session.query(LikeQuest).filter(LikeQuest.id_quest == id_quest).filter(LikeQuest.cool).all()
        dislikes = db.session.query(LikeQuest).filter(LikeQuest.id_quest == id_quest).filter(LikeQuest.cool == False).all()
        return len(likes) - len(dislikes)

    @staticmethod
    def clean_table_in_finish_tournament():
        if Tournament.is_active_tournament():
            LikeQuest.query.delete()
            return True
        else:
            return False
