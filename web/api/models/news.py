from web import db
from flask_login import UserMixin


class News(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(256))
    content = db.Column(db.String(1024))
    time = db.Column(db.String(256))
    author = db.Column(db.String(256))
    type = db.Column(db.String(256))
    tournament_id = db.Column(db.Integer)

    def __init__(self, title, content, time, author, type, tournament_id):
        self.title = title
        self.content = content
        self.time = time
        self.author = author
        self.type = type
        self.tournament_id = tournament_id

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id

    @staticmethod
    def get_all_reverse():
        all_news = News.query.all()
        return all_news[::-1]
