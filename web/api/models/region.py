from flask_login import UserMixin
from web import db


class Region(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256))
    description = db.Column(db.String(4096))
    leader = db.Column(db.String(256))
    quote = db.Column(db.String(512))
    author_q = db.Column(db.String(216))

    def __init__(self, name, description, leader='',
                 quote='То, что нам иногда кажется суровым испытанием, может обернуться неожиданной удачей.',
                 author_q='Оскар Уайльд'):
        self.name = name
        self.description = description
        self.leader = leader
        self.quote = quote
        self.author_q = author_q

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id

    @staticmethod
    def update_by_name(name, key, value):
        db.session.query(Region).filter(Region.name == name).update({key: value}, synchronize_session='evaluate')
        db.session.commit()

    @staticmethod
    def get_all():
        return Region.query.all()
