from flask_login import UserMixin
from web import db


class Post(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    html = db.Column(db.Text)
    region = db.Column(db.String(256))
    author = db.Column(db.String(256))
    time = db.Column(db.String(256))
    image = db.Column(db.String(256))

    def __init__(self, html, region, author, time, image):
        self.html = html
        self.region = region
        self.author = author
        self.time = time
        self.image = image

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id

    @staticmethod
    def get_all():
        return Post.query.all()

    @staticmethod
    def update_by_id(id, key, value):
        db.session.query(Post).filter(Post.id == id).update({key: value}, synchronize_session='evaluate')
        db.session.commit()

    @staticmethod
    def get_posts_in_region(region):
        posts = Post.get_all()
        p = list()
        for i in posts:
            if i.region == region:
                p.append(i)
        return p

    @staticmethod
    def check_photo_file_name(filename):
        if db.session.query(Post).filter(Post.image == filename).first():
            return False
        return True
