from web import db
from flask_login import UserMixin


class Answer(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(256))
    room_id = db.Column(db.Integer, db.ForeignKey('room.id'))
    next_room_id = db.Column(db.Integer)
    next_room_code = db.Column(db.String(256))

    def __init__(self, content, room_id, next_room_id=0, next_room_code='lol'):
        self.content = content
        self.room_id = room_id
        self.next_room_id = next_room_id
        self.next_room_code = next_room_code

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id

    @staticmethod
    def get_all():
        return Answer.query.all()

    @staticmethod
    def get_correct_answers(room_id):
        return db.session.query(Answer).filter(Answer.room_id == room_id)

    @staticmethod
    def get_variants_in_room(room_id):
        return db.session.query(Answer).filter(Answer.room_id == room_id).all()

    @staticmethod
    def update_by_id(id, key, value):
        db.session.query(Answer).filter(Answer.id == id).update({key: value}, synchronize_session='evaluate')
        db.session.commit()
