from web import db
from flask_login import UserMixin


class LikePost(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(256))
    id_post = db.Column(db.Integer)
    cool = db.Column(db.Boolean)

    def __init__(self, login, id_post, cool):
        self.login = login
        self.id_post = id_post
        self.cool = cool

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id

    @staticmethod
    def get_all():
        return LikePost.query.all()

    @staticmethod
    def get_all_likes_to_post(id_post):
        likes = db.session.query(LikePost).filter(LikePost.id_post == id_post).filter(LikePost.cool).all()
        dislikes = db.session.query(LikePost).filter(LikePost.id_post == id_post).filter(LikePost.cool == False).all()
        return len(likes) - len(dislikes)

    @staticmethod
    def check_is_post_liked(login, id_post):
        if db.session.query(LikePost).filter(LikePost.login == login).filter(LikePost.id_post == id_post).first():
            return True

    @staticmethod
    def get_what_choose_user(login, id_post):
        return db.session.query(LikePost).filter(LikePost.login == login).filter(LikePost.id_post == id_post).first().cool
