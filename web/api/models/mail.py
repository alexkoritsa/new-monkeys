from web import db
from sqlalchemy import or_
from flask_login import UserMixin


class Mail(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(256))
    content = db.Column(db.String(2048))
    time = db.Column(db.String(256))
    author = db.Column(db.String(256))
    target = db.Column(db.String(256))
    kind = db.Column(db.String(256))
    photo = db.Column(db.String(256))
    is_read = db.Column(db.Boolean)

    def __init__(self, title, content, time, author, target, kind, photo, is_read=False):
        self.title = title
        self.content = content
        self.time = time
        self.author = author
        self.target = target
        self.kind = kind
        self.photo = photo
        self.is_read = is_read

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id

    @staticmethod
    def get_all():
        return Mail.query.all()

    @staticmethod
    def get_all_mail_for_user(login):
        return db.session.query(Mail).filter(Mail.target == login).all()

    @staticmethod
    def get_how_much_unread_mail_for_user(login):
        return len(db.session.query(Mail).filter(Mail.target == login).filter(Mail.is_read == False).all())

    @staticmethod
    def get_all_sent_user_mail(login):
        return db.session.query(Mail).filter(Mail.author == login).all()

    @staticmethod
    def get_all_mail_for_admin():
        mails = db.session.query(Mail).filter(or_(Mail.kind == 'SUPPORT', Mail.kind == 'LEADER')).all()
        return mails[::-1]

    @staticmethod
    def get_all_mail_for_leader():
        return db.session.query(Mail).filter(Mail.king == 'LEADER').all()

    @staticmethod
    def update_by_id(id, key, value):
        db.session.query(Mail).filter(Mail.id == id).update({key: value}, synchronize_session='evaluate')
        db.session.commit()
