from flask import Blueprint

api = Blueprint('api', __name__, template_folder='../templates', static_folder='../static')

from web.api.controllers.MainPageController import MainPageController
from web.api.controllers.user.RegController import RegController
from web.api.controllers.user.LoginController import LoginController
from web.api.controllers.user.LogoutController import LogoutController
from web.api.controllers.user.AccountController import AccountController
from web.api.controllers.user.ResetPasswordController import ResetPasswordController
from web.api.controllers.user.EditProfileInfoControllers import EditNameController, EditEmailController, EditPasswordController, EditAvatarController
from web.api.controllers.user.GetAllUsersController import GetAllUsersController
from web.api.controllers.rules.AddLeaderController import AddLeaderController, AddLeaderPageController
from web.api.controllers.rules.DeleteLeaderController import DeleteLeaderController
from web.api.controllers.rules.BanUserController import BanUserController
from web.api.controllers.rules.UnbanUserController import UnbanUserController
from web.api.controllers.team.AllTeamsController import AllTeamsController
from web.api.controllers.user.ShowOtherUserController import ShowOtherUserController
from web.api.controllers.errors.RoleAndThingErrorController import RoleErrorController, ThingErrorController
from web.api.controllers.team.JoinAndLeaveTeamController import JoinTeamController, LeaveTeamController
from web.api.controllers.team.ShowTeamToUserController import ShowTeamToUserController
from web.api.controllers.team.NewSecretTeamCodeController import NewSecretTeamCodeController
from web.api.controllers.team.EditTeamInfoControllers import EditTeamNameController, EditTeamAvatarController, EditTeamPasswordController
from web.api.controllers.quest.QuestsPageController import QuestsPageController
from web.api.controllers.quest.QuestPageController import QuestPageController
from web.api.controllers.room.CreateRoomController import CreateRoomController
from web.api.controllers.room.PassRoomController import PassRoomController
from web.api.controllers.room.ErrorNextRoomController import ErrorNextRoomController
from web.api.controllers.room.EditRoomInfoController import EditTitleRoomController, EditContentRoomController, EditThingsRoomController, EditPhotoRoomController
from web.api.controllers.quest.FinishQuestController import FinishQuestController
from web.api.controllers.quest.EditQuestInfoController import EditQuestTitleController, EditQuestRatingController
from web.api.controllers.quest.AlreadyPassedQuestController import AlredyPassedQuestController
from web.api.controllers.room.DeleteRoomAndQuestController import DeleteQuestController, DeleteRoomController
from web.api.controllers.TournamentPageController import TournamentPageController
from web.api.controllers.rules.WorkWithAdminController import AddAdminController, DeleteAdminController
from web.api.controllers.ScoreboardController import ScoreboardController
from web.api.controllers.errors.Show404Controller import Show404Controller
from web.api.controllers.errors.ShowUsersErrorsController import ShowUsersErrorsController
from web.api.controllers.errors.ShowServerErrorsController import ShowServerErrorsController
from web.api.controllers.errors.BrutforceController import BrutforceController
from web.api.controllers.SupportController import WriteSupportController, ReadSupportController
from web.api.controllers.quest.LikesController import LikeController, DislikeController
from web.api.controllers.mail.AnswerMailSupportController import AnswerSupportController
from web.api.controllers.mail.UserMailController import ShowMailPageController, AnswerMailPageController, \
    WriteUserMailController, ShowAllSentMailController, ShowMyMailController
from web.api.controllers.AboutUsPageController import AboutUsPageController
from web.api.controllers.NewsController import NewsController
from web.api.controllers.tournament.ChangeStatusTournamentController import StartTournamentController, \
    StopTournamentController
from web.api.controllers.errors.TournamentErrorsController import ErrorStartTournamentController, \
    ErrorStopTournamentController, ErrorTournamentController
from web.api.controllers.tournament.AboutEachTournamentController import AboutEachTournamentController
from web.api.controllers.quest.MarkQuestController import MarkQuestController
from web.api.controllers.region.ShowMyRegionController import ShowMyRegionController
from web.api.controllers.region.EditMyRegionController import EditMyRegionController
from web.api.controllers.region.EditPostController import ChangeStorylineController
from web.api.controllers.region.DeletePostController import DeletePostController
from web.api.controllers.region.AddPostByAdminController import AddPostByAdminController
from web.api.controllers.region.AllWorldsPageController import AllWorldsPageController
from web.api.controllers.region.ShowOtherRegionController import ShowOtherRegionController
from web.api.controllers.region.PostPageController import PostPageController
from web.api.controllers.region.LikeAndDislikePostControllers import LikePostController, DislikePostController
from web.api.controllers.region.ChangeQuoteController import ChangeQuoteController
from web.api.controllers.region.ChageDescriprionController import ChangeDescriptionController


'''
USER AND WORK WITH USER
'''
api.add_url_rule('/', view_func=MainPageController.as_view('MainPage'))
api.add_url_rule('reg', view_func=RegController.as_view('Registration'))
api.add_url_rule('login', view_func=LoginController.as_view('Login'))
api.add_url_rule('logout', view_func=LogoutController.as_view('Logout'))
api.add_url_rule('account', view_func=AccountController.as_view('Account'))
api.add_url_rule('reset', view_func=ResetPasswordController.as_view('ResetPassword'))
api.add_url_rule('edit/name', view_func=EditNameController.as_view('EditName'))
api.add_url_rule('edit/email', view_func=EditEmailController.as_view('EditEmail'))
api.add_url_rule('edit/password', view_func=EditPasswordController.as_view('EditPassword'))
api.add_url_rule('edit/avatar', view_func=EditAvatarController.as_view('EditAvatar'))


'''
REGIONS
'''
api.add_url_rule('myworld', view_func=ShowMyRegionController.as_view('ShowMyRegion'))
api.add_url_rule('my/world/edit', view_func=EditMyRegionController.as_view('EditMyRegion'))
api.add_url_rule('edit/post/<int:id>', view_func=ChangeStorylineController.as_view('EditStorylineController'))
api.add_url_rule('delete/post/<int:id>', view_func=DeletePostController.as_view('DeletePostController'))
api.add_url_rule('add/post', view_func=AddPostByAdminController.as_view('AddPostByAdminController'))
api.add_url_rule('worlds', view_func=AllWorldsPageController.as_view('AllWorldsController'))
api.add_url_rule('world/<int:id>', view_func=ShowOtherRegionController.as_view('ShowOtherRegionController'))
api.add_url_rule('world/edit/quote/<int:id>', view_func=ChangeQuoteController.as_view('ChangeQuoteController'))
api.add_url_rule('world/edit/description/<int:id>',
                 view_func=ChangeDescriptionController.as_view('ChangeDescriptionRegionController'))


'''
POSTS
'''
api.add_url_rule('post/<int:id_region>/<int:loop_index>', view_func=PostPageController.as_view('PostPageController'))
api.add_url_rule('post/like/<int:id_post>/<int:loop_index>', view_func=LikePostController.as_view('LikePostController'))
api.add_url_rule('post/dislike/<int:id_post>/<int:loop_index>', view_func=DislikePostController.as_view('DislikePostController'))


'''
MAIL
'''
api.add_url_rule('mail', view_func=ShowMailPageController.as_view('ShowUserMailPage'))
api.add_url_rule('mail/<int:id_mail>', view_func=AnswerMailPageController.as_view('AnswerMailPage'))
api.add_url_rule('mail/new', view_func=WriteUserMailController.as_view('WriteUserMailPage'))
api.add_url_rule('mail/sent', view_func=ShowAllSentMailController.as_view('ShowAllSentMailsPage'))
api.add_url_rule('mail/sent/<int:id_mail>', view_func=ShowMyMailController.as_view('ShowMyMailPage'))


'''
OTHER PAGES
'''
api.add_url_rule('users', view_func=GetAllUsersController.as_view('AllUsers'))
api.add_url_rule('teams', view_func=AllTeamsController.as_view('AllTeams'))
api.add_url_rule('users/<string:login>', view_func=ShowOtherUserController.as_view('ShowOtherUser'))
api.add_url_rule('quests', view_func=QuestsPageController.as_view('QuestsPage'))
api.add_url_rule('scoreboard', view_func=ScoreboardController.as_view('ScoreboardPage'))
api.add_url_rule('contact', view_func=AboutUsPageController.as_view('AboutUsPage'))
api.add_url_rule('news', view_func=NewsController.as_view('NewsPageController'))


'''
TOURNAMENT
'''
api.add_url_rule('tournament', view_func=TournamentPageController.as_view('TournamentPage'))
api.add_url_rule('tournament/start', view_func=StartTournamentController.as_view('StartTournamentPage'))
api.add_url_rule('tournament/finish', view_func=StopTournamentController.as_view('StopTournamentPage'))
api.add_url_rule('tournament/info/<int:id>', view_func=AboutEachTournamentController.as_view('AboutEachTournamentPage'))

'''
SUPPORT
'''
api.add_url_rule('write/support', view_func=WriteSupportController.as_view('WriteSupport'))
api.add_url_rule('show/support', view_func=ReadSupportController.as_view('ReadSupport'))
api.add_url_rule('support/mail/<int:id_mail>', view_func=AnswerSupportController.as_view('AnswerSupport'))


'''
ADMIN PANEL AND ACCOUNT
'''
api.add_url_rule('admin/leader/<int:id>', view_func=AddLeaderPageController.as_view('AddLeaderPage'))
api.add_url_rule('admin/add/leader/<int:id>/<string:region>', view_func=AddLeaderController.as_view('AddLeaderController'))
api.add_url_rule('admin/delete/leader/<int:id>', view_func=DeleteLeaderController.as_view('DeleteLeader'))
api.add_url_rule('admin/ban/user/<int:id>', view_func=BanUserController.as_view('BanUser'))
api.add_url_rule('admin/unban/user/<int:id>', view_func=UnbanUserController.as_view('UnbanUser'))
api.add_url_rule('admin/add/admin/<int:id>', view_func=AddAdminController.as_view('AddAdmin'))
api.add_url_rule('admin/delete/admin/<int:id>', view_func=DeleteAdminController.as_view('DeleteAdmin'))


'''
TEAMS AND WORK WITH TEAMS
'''
api.add_url_rule('add/<int:id_team>/<int:id_user>', view_func=JoinTeamController.as_view('JoinTeam'))
api.add_url_rule('leave/<int:id_team>/<int:id_user>', view_func=LeaveTeamController.as_view('LeaveTeam'))
api.add_url_rule('team/user/<int:id>', view_func=ShowTeamToUserController.as_view('ShowTeamToUser'))
api.add_url_rule('edit/team/code/<int:id>', view_func=NewSecretTeamCodeController.as_view('NewSecretTeamCode'))
api.add_url_rule('edit/team/name/<int:id>', view_func=EditTeamNameController.as_view('EditTeamName'))
api.add_url_rule('edit/team/avatar/<int:id>', view_func=EditTeamAvatarController.as_view('EditTeamAvatar'))
api.add_url_rule('edit/team/mycode/<int:id>', view_func=EditTeamPasswordController.as_view('EditTeamPassword'))


'''
QUESTS
'''
api.add_url_rule('quest/<int:id>', view_func=QuestPageController.as_view('SingleQuestPage'))
api.add_url_rule('quest/<int:id>/create_room', view_func=CreateRoomController.as_view('CreateRoom'))
api.add_url_rule('quest/pass/<int:quest_id>/<string:room_code>', view_func=PassRoomController.as_view('PassRoom'))
api.add_url_rule('quest/finish', view_func=FinishQuestController.as_view('FinishQuest'))
api.add_url_rule('quest/already/passed', view_func=AlredyPassedQuestController.as_view('AlreadyPassedQuest'))
api.add_url_rule('quest/delete/<int:id_quest>', view_func=DeleteQuestController.as_view('DeleteQuest'))
api.add_url_rule('quest/like', view_func=LikeController.as_view('LikeQuest'))
api.add_url_rule('quest/dislike', view_func=DislikeController.as_view('DislikeQuest'))
api.add_url_rule('quest/mark', view_func=MarkQuestController.as_view('MarkQuestsController'))


'''
ROOMS
'''
api.add_url_rule('quest/room/edit/title/<int:id_quest>/<int:id_room>', view_func=EditTitleRoomController.as_view('EditTitleRoom'))
api.add_url_rule('quest/room/edit/content/<int:id_quest>/<int:id_room>', view_func=EditContentRoomController.as_view('EditContentRoom'))
api.add_url_rule('quest/room/edit/things/<int:id_quest>/<int:id_room>', view_func=EditThingsRoomController.as_view('EditThingsRoom'))
api.add_url_rule('quest/room/edit/photo/<int:id_quest>/<int:id_room>', view_func=EditPhotoRoomController.as_view('EditPhotoRoom'))
api.add_url_rule('quest/edit/title/<int:id_quest>', view_func=EditQuestTitleController.as_view('EditTitleQuest'))
api.add_url_rule('quest/edit/rating/<int:id_quest>', view_func=EditQuestRatingController.as_view('EditRatingQuest'))
api.add_url_rule('quest/room/delete/<int:id_quest>/<int:id_room>', view_func=DeleteRoomController.as_view('DeleteRoom'))


'''
DIFFERENT ERRORS
'''
api.add_url_rule('error/things', view_func=ThingErrorController.as_view('ThingError'))
api.add_url_rule('error/right', view_func=RoleErrorController.as_view('RoleError'))
api.add_url_rule('quest/pass/<int:id_quest>/lol', view_func=ErrorNextRoomController.as_view('ErrorNextRoom'))
api.add_url_rule('error/tournament', view_func=ErrorTournamentController.as_view('ErrorRightTournamentController'))
api.add_url_rule('error/start/tournament', view_func=ErrorStartTournamentController.as_view('ErrorStartTournament'))
api.add_url_rule('error/finish/tournament', view_func=ErrorStopTournamentController.as_view('ErrorStopTournament'))


'''
SHOW ERRORS IN ADMIN PANEL
'''
api.add_url_rule('error/show/404', view_func=Show404Controller.as_view('Show404Page'))
api.add_url_rule('error/show/users', view_func=ShowUsersErrorsController.as_view('ShowUsersErrorsPage'))
api.add_url_rule('error/show/server', view_func=ShowServerErrorsController.as_view('ShowServerErrorsPage'))
api.add_url_rule('error/show/brutforce', view_func=BrutforceController.as_view('BrutforceErrorPage'))
