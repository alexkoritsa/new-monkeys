from flask import render_template, redirect, request
from flask.views import MethodView
from web.api.models.team import Team
from web.api.models.user import User
from web.api.forms.AvatarForm import AvatarForm
from web import db
from web import ALLOWED_EXTENSIONS, app
import os
import random
from werkzeug.utils import secure_filename
from flask_login import current_user, login_required


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


class ShowTeamToUserController(MethodView):
    @login_required
    def get(self, id):
        form1 = AvatarForm()
        action = '/team/user/' + str(id)
        login = current_user.login
        user = db.session.query(User).filter(User.login == login).first()

        team = db.session.query(Team).filter(Team.id == id).first()
        avatar = 'photos/uploads/' + team.photo
        players = User.get_how_much_players_in_one_team(id)
        return render_template('team.html', team=team, form1=form1, action=action, avatar=avatar, players=players, user=user)

    @login_required
    def post(self, id):
        form1 = AvatarForm()
        team = db.session.query(Team).filter(Team.id == id).first()
        avatar = 'photos/uploads/' + team.photo
        action = '/team/user/' + str(id)
        players = User.get_how_much_players_in_one_team(id)
        login = current_user.login
        user = db.session.query(User).filter(User.login == login).first()

        if form1.submit_photo.data and form1.validate_on_submit():
            file = request.files['file']
            if file and allowed_file(file.filename):
                code = ''.join([random.choice(list('123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM')) for x in range(8)])
                filename = code + team.password + secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                Team.update_by_id(id, 'photo', filename)
                return redirect(action)
            return render_template('team.html', form1=form1, team=team, error_text_photo='Что-то пошло не так. Попробуйте еще раз.', action=action, avatar=avatar, players=players, user=user)
        return render_template('team.html', form1=form1, team=team, action=action, avatar=avatar, players=players, user=user)
