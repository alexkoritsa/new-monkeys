from flask import redirect
from flask.views import MethodView
from web.api.models.team import Team
from web import db
import random
from flask_login import login_required, current_user


class NewSecretTeamCodeController(MethodView):
    @login_required
    def get(self, id):
        action = '/team/user/' + str(id)

        team = db.session.query(Team).filter(Team.id == id).first()
        if current_user.login != team.creator:
            return redirect('/error/right')

        code = ''.join([random.choice(list('123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM')) for x in range(8)])
        Team.update_by_id(id, 'password', code)
        return redirect(action)
