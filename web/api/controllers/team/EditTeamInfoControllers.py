from flask import redirect, render_template, request
from flask.views import MethodView
from web import db
from web.api.models.team import Team
from web.api.forms.EditTeamInfoForms import EditTeamNameForm, EditTeamPasswordForm
from web.api.forms.AvatarForm import AvatarForm
import random
from web import ALLOWED_EXTENSIONS, app
import os
from werkzeug.utils import secure_filename
from flask_login import current_user, login_required


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


class EditTeamPasswordController(MethodView):
    @login_required
    def get(self, id):
        team = db.session.query(Team).filter(Team.id == id).first()
        myTeam = '/team/user/' + str(id)
        action = '/edit/team/mycode/' + str(id)

        if current_user.team_id != team.id:
            return redirect('/error/right')

        form = EditTeamPasswordForm()
        return render_template('editing/editTeamPassword.html', form=form, myTeam=myTeam, action=action, team=team)

    @login_required
    def post(self, id):
        team = db.session.query(Team).filter(Team.id == id).first()
        if current_user.team_id != team.id:
            return redirect('/error/right')

        myTeam = '/team/user/' + str(id)
        action = '/edit/team/mycode/' + str(id)

        form = EditTeamPasswordForm()
        if form.validate_on_submit():
            Team.update_by_id(id, 'password', form.password.data)
            return render_template('editing/editTeamPassword.html', form=form, success_text='Пароль успешно изменен!',
                                   myTeam=myTeam, action=action, team=team)
        return render_template('editing/editTeamPassword.html', form=form, myTeam=myTeam, action=action, team=team)


class EditTeamNameController(MethodView):
    @login_required
    def get(self, id):
        team = db.session.query(Team).filter(Team.id == id).first()
        if current_user.team_id != team.id:
            return redirect('/error/right')

        form = EditTeamNameForm()
        myTeam = '/team/user/' + str(id)
        action = '/edit/team/name/' + str(id)
        return render_template('editing/editTeamName.html', form=form, myTeam=myTeam, action=action, team=team)

    @login_required
    def post(self, id):
        team = db.session.query(Team).filter(Team.id == id).first()
        if current_user.team_id != team.id:
            return redirect('/error/right')

        form = EditTeamNameForm()
        myTeam = '/team/user/' + str(id)
        action = '/edit/team/name/' + str(id)

        if form.validate_on_submit():
            Team.update_by_id(id, 'name', form.name.data)
            return render_template('editing/editTeamName.html', form=form, myTeam=myTeam, action=action,
                                   success_text='Название изменено успешно!', team=team)
        return render_template('editing/editTeamName.html', form=form, myTeam=myTeam, action=action, team=team)


class EditTeamAvatarController(MethodView):
    @login_required
    def get(self, id):
        team = db.session.query(Team).filter(Team.id == id).first()
        myTeam = '/team/user/' + str(id)
        action = '/edit/team/avatar/' + str(id)

        if current_user.team_id != team.id:
            return redirect('/error/right')

        form = AvatarForm()
        avatar = 'photos/uploads/' + team.photo
        return render_template('editing/editTeamAvatar.html', form=form, avatar=avatar, photo=team.photo, team=team,
                               myTeam=myTeam, action=action)

    @login_required
    def post(self, id):
        form = AvatarForm()
        team = db.session.query(Team).filter(Team.id == id).first()
        avatar = 'photos/uploads/' + team.photo
        myTeam = '/team/user/' + str(id)
        action = '/edit/team/avatar/' + str(id)

        if current_user.team_id != team.id:
            return redirect('/error/right')

        if form.submit_photo.data and form.validate_on_submit():
            file = request.files['file']
            if file and allowed_file(file.filename):
                code = ''.join([random.choice(list('123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM')) for x in range(8)])
                filename = code + team.password + secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                Team.update_by_id(id, 'photo', filename)
                return redirect(action)
            return render_template('editing/editTeamAvatar.html', form=form, avatar=avatar,
                                   error_text='Что-то пошло не так. Попробуйте еще раз.', photo=team.photo, team=team,
                                   myTeam=myTeam, action=action)
        return render_template('editing/editTeamAvatar.html', form=form, avatar=avatar, photo=team.photo, team=team,
                               myTeam=myTeam, action=action)