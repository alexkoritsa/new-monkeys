from flask.views import MethodView
from flask import render_template
from web.api.models.team import Team
from web.api.models.user import User
from web.api.forms.SecretCodeTeamForm import SecretCodeTeamForm
from web import db
from flask_login import current_user, login_required


class AllTeamsController(MethodView):
    @login_required
    def get(self):
        form = SecretCodeTeamForm()
        all_teams = Team.get_all()
        login = current_user.login
        user = db.session.query(User).filter(User.login == login).first()
        return render_template('teams.html', all_teams=all_teams, user=user, form=form)
