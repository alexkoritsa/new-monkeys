from flask import redirect, render_template, request
from flask.views import MethodView
from web import db
from web.api.models.user import User
from web.api.models.team import Team
from web.api.models.userToTeam import UserToTeam
from web.api.forms.SecretCodeTeamForm import SecretCodeTeamForm
from web.api.models.log import Log
import datetime
from flask_login import login_required, current_user


class JoinTeamController(MethodView):
    @login_required
    def get(self, id_team, id_user):
        login = current_user.login
        user = db.session.query(User).filter(User.login == login).first()
        form = SecretCodeTeamForm()
        return render_template('joinTeam.html', form=form)

    @login_required
    def post(self, id_team, id_user):
        login = current_user.login
        user = db.session.query(User).filter(User.login == login).first()
        if id_user != user.id:
            return redirect('/error/right')

        team = db.session.query(Team).filter(Team.id == id_team).first()
        all_teams = Team.get_all()

        if user.team_id != 0:
            return redirect('/error/right')

        form = SecretCodeTeamForm()
        time = datetime.datetime.now().strftime("%H:%M %d.%m.%Y")
        if form.validate_on_submit():
            if form.code.data == team.password:
                User.update_by_login(login, 'team_id', id_team)
                joinUser = UserToTeam(user.id, id_team, True)
                joinUser.save()
                return redirect('/teams')
        log = Log('WRONG PASS TEAM', request.path, time, current_user.login)
        log.save()
        return render_template('teams.html', error_text='Неправильный секретный код.', form=form, all_teams=all_teams, user=user)


class LeaveTeamController(MethodView):
    @login_required
    def get(self, id_team, id_user):
        login = current_user.login
        user = db.session.query(User).filter(User.login == login).first()
        if id_user != user.id:
            return redirect('/error/right')

        if user.team_id == 0:
            return redirect('/error/right')

        User.update_by_login(login, 'team_id', 0)
        UserToTeam.query.filter_by(id_user=user.id).filter_by(now=True).filter_by(id_team=id_team).update(dict(now=False))
        db.session.commit()
        return redirect('/teams')
