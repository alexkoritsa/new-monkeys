from flask.views import MethodView
from flask import render_template, redirect
from web import db
from web.api.models.user import User
from flask_login import login_required, current_user


class DeleteLeaderController(MethodView):
    @login_required
    def get(self, id):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        all_users = User.get_all()
        client = db.session.query(User).filter(User.id == id).first()

        if user.role != 'admin':
            return redirect('/error/right')

        if client.role == 'user' or client.role == 'ban':
            return render_template('account.html', user=user, all_users=all_users, error_text='У этого пользоваеля еще нет прав вожака.')

        if client.role == 'admin':
            return render_template('accout.html', user=user, all_users=all_users, error_text='Этот пользователь админ.')

        if id == user.id:
            return render_template('account.html', user=user, all_users=all_users, error_text='Себя лишить прав, которых у Вас нет? Смело, но нет.')

        User.query.filter_by(id=id).update(dict(role='user'))
        db.session.commit()
        return redirect('/account')
