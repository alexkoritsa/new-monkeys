from flask.views import MethodView
from flask import render_template, redirect, request, url_for
from web.api.models.user import User
from web import db
from flask_login import login_required, current_user
from web.api.models.region import Region


def get_region(region):
    areas = {
        '1': 'Математики',
        '2': 'Физики и химики',
        '3': 'Астрономы',
        '4': 'Спортсмены',
        '5': 'Игроки',
        '6': 'Географы и историки',
        '7': 'Биологи и генетики',
        '8': 'Тролли',
        '9': 'Программисты',
        '10': 'Взломщики',
        '11': 'Оккультисты',
        '12': 'Творцы'
    }
    return areas[region]


class AddLeaderController(MethodView):
    @login_required
    def get(self, id, region):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        all_users = User.get_all()
        client = db.session.query(User).filter(User.id == id).first()

        if user.role != 'admin':
            return redirect('/error/right')

        if client.role == 'leader' or client.role == 'ban':
            return render_template('account.html', error_text='Этот пользователь не может стать вожаком. '
                                                              'Он забанен или уже является вожаком.', user=user,
                                   all_users=all_users)

        if client.role == 'admin':
            return render_template('account.html', error_text='Этот пользователь - админ. Так нельзя.', user=user,
                                   all_users=all_users)

        User.query.filter_by(id=id).update(dict(role='leader'))
        db.session.commit()
        Region.update_by_name(region, 'leader', client.login)
        return redirect('/account')


class AddLeaderPageController(MethodView):
    @login_required
    def get(self, id):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        all_users = User.get_all()
        client = db.session.query(User).filter(User.id == id).first()

        if user.role != 'admin':
            return redirect('/error/right')

        if client.role == 'leader' or client.role == 'ban':
            return render_template('account.html', error_text='Этот пользователь не может стать вожаком. '
                                                              'Он забанен или уже является вожаком.', user=user,
                                   all_users=all_users)

        if client.role == 'admin':
            return render_template('account.html', error_text='Этот пользователь - админ. Так нельзя.', user=user,
                                   all_users=all_users)
        return render_template('add_leader_page.html', client=client)

    @login_required
    def post(self, id):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        all_users = User.get_all()
        client = db.session.query(User).filter(User.id == id).first()

        if user.role != 'admin':
            return redirect('/error/right')

        if client.role == 'leader' or client.role == 'ban':
            return render_template('account.html', error_text='Этот пользователь не может стать вожаком. '
                                                              'Он забанен или уже является вожаком.', user=user,
                                   all_users=all_users)

        if client.role == 'admin':
            return render_template('account.html', error_text='Этот пользователь - админ. Так нельзя.', user=user,
                                   all_users=all_users)

        if 'make-new-leader' in request.form:
            region_leader = request.form['select_region_for_leader']
            print(region_leader)
            final_region = get_region(region_leader)
            print(final_region)
            check_region = db.session.query(Region).filter(Region.name == final_region).first()
            if check_region.leader != '':
                error_text_string = 'У этого края уже есть вожак - ' + str(check_region.leader) + \
                                    '. Его сначала нужно удалить и тогда Вы сможете добавить нового!'
                return render_template('add_leader_page.html', error_text_string=error_text_string, client=client)
            return redirect(url_for('api.AddLeaderController', id=id, region=final_region))
