from flask.views import MethodView
from flask import render_template, redirect
from web.api.models.user import User
from web import db
from flask_login import login_required, current_user
from web.api.models.quest import Quest
from web.api.models.team import Team
from web.api.models.finishQuest import FinishQuest
from web.api.forms.AddTeamForm import AddTeamForm
from web.api.forms.AvatarForm import AvatarForm


class BanUserController(MethodView):
    @login_required
    def get(self, id):
        if current_user.role != 'admin':
            return redirect('/error/right')

        user = db.session.query(User).filter(User.login == current_user.login).first()
        all_users = User.get_all()
        client = db.session.query(User).filter(User.id == id).first()

        form = AddTeamForm()
        form1 = AvatarForm()
        login = current_user.login
        user = db.session.query(User).filter(User.login == login).first()
        team = db.session.query(Team).filter(Team.id == user.team_id).first()
        myTeam = ''
        all_users = User.get_all()
        all_quests = Quest.get_all()
        avatar = 'photos/uploads/' + user.photo

        all_scores = list()
        teams = Team.get_all()
        for i in teams:
            final_score = FinishQuest.get_score_team(i.id)
            need_save = str(final_score) + '|' + i.name
            all_scores.append(need_save)

        new_list = list()
        final = list()
        for i in range(len(all_scores)):
            new_list = all_scores[i].split('|')
            final.append(new_list)

        final = sorted(final, reverse=True)

        if user.team_id != 0:
            myTeam = team.name
            team_avatar = 'photos/uploads/' + team.photo

        if client.role == 'ban':
            return render_template('account.html', all_users=all_users, user=user, form=form, form1=form1, avatar=avatar,
                                   myTeam=myTeam, team=team, team_avatar=team_avatar, all_quests=all_quests, final=final,
                                   error_text='Этот пользователь уже забанен.')
        if id == current_user.id:
            return render_template('account.html', all_users=all_users, user=user, form=form, form1=form1, avatar=avatar,
                                   myTeam=myTeam, team=team, team_avatar=team_avatar, all_quests=all_quests, final=final,
                                   error_text='Вы не можете забанить себя')
        if client.role == 'admin':
            return render_template('account.html', all_users=all_users, user=user, form=form, form1=form1, avatar=avatar,
                                   myTeam=myTeam, team=team, team_avatar=team_avatar, all_quests=all_quests, final=final,
                                   error_text='Вы не можете забанить админа.')

        User.query.filter_by(id=id).update(dict(role='ban'))
        db.session.commit()
        return render_template('account.html', all_users=all_users, user=user, form=form, form1=form1, avatar=avatar,
                               myTeam=myTeam, team=team, team_avatar=team_avatar, all_quests=all_quests, final=final,
                               success_text='Пользователь забанен!')
