from flask import redirect, render_template
from flask.views import MethodView
from web import db
from web.api.models.user import User
from flask_login import login_required, current_user


class AddAdminController(MethodView):
    @login_required
    def get(self, id):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        all_users = User.get_all()
        client = db.session.query(User).filter(User.id == id).first()

        if user.role != 'admin':
            return redirect('/error/right')

        if client.role == 'ban' or client.role == 'admin':
            return render_template('account.html', user=user, all_users=all_users,
                                   error_text='Этот пользователь не может стать админом.')

        User.query.filter_by(id=id).update(dict(role='admin'))
        db.session.commit()
        return redirect('/account')


class DeleteAdminController(MethodView):
    @login_required
    def get(self, id):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        client = db.session.query(User).filter(User.id == id).first()

        if user.role != 'admin':
            return redirect('/error/right')

        if client.id != user.id:
            return redirect('/error/right')

        if user.role == 'admin':
            User.query.filter_by(id=id).update(dict(role='leader'))
            db.session.commit()
            return redirect('/account')
