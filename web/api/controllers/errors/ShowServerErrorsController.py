from flask.views import MethodView
from flask import render_template, redirect
from web import db
from web.api.models.user import User
from web.api.models.log import Log
import datetime
from flask_login import login_required, current_user


class ShowServerErrorsController(MethodView):
    @login_required
    def get(self):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        if user.role != 'admin':
            return redirect('/error/right')

        errors = Log.get_server_errors()

        now = datetime.datetime.now()
        # График 404 ошибки
        not_reverse_error_404 = Log.get_404_errors_not_reverse()

        labels_404 = list()
        for i in not_reverse_error_404:
            if str(now.day) == i.time[6:8]:
                if i.time[:1] == '0':
                    labels_404.append(i.time[1:2])
                labels_404.append(i.time[:2])

        numbers_404 = list()
        for i in range(24):
            numbers_404.append(labels_404.count(str(i)))

        # График ошибки сервера
        not_reverse_server_error = Log.get_server_errors_not_reverse()

        labels_server = list()
        for i in not_reverse_server_error:
            print(i.time[:1])
            if str(now.day) == i.time[6:8]:
                if i.time[:1] == '0':
                    labels_server.append(i.time[1:2])
                labels_server.append(i.time[:2])

        numbers_server = list()
        for i in range(24):
            numbers_server.append(labels_server.count(str(i)))

        # График ошибки пользователей
        not_reverse_users_error = Log.get_users_errors_not_reverse()

        labels_user = list()
        for i in not_reverse_users_error:
            if str(now.day) == i.time[6:8]:
                if i.time[:1] == '0':
                    labels_user.append(i.time[1:2])
                labels_user.append(i.time[:2])

        numbers_users = list()
        for i in range(24):
            numbers_users.append(labels_user.count(str(i)))

        return render_template('errors/show_server_errors.html', errors=errors, data_404=numbers_404, data_server=numbers_server,
                               data_users=numbers_users)
