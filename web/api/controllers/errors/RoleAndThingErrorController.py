from flask import render_template
from flask.views import MethodView


class RoleErrorController(MethodView):
    def get(self):
        return render_template('errors/role_error.html')


class ThingErrorController(MethodView):
    def get(self):
        return render_template('errors/things_error.html')
