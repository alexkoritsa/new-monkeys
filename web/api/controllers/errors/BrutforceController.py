from flask.views import MethodView
from flask import render_template, redirect, request
from web import db
from web.api.models.user import User
from web.api.models.attempt import Attempt
from web.api.models.team import Team
from web.api.models.room import Room
import datetime
from flask_login import login_required, current_user


class BrutforceController(MethodView):
    @login_required
    def get(self):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        if user.role != 'admin':
            return redirect('/error/right')
        attempts = Attempt.get_all()
        return render_template('errors/brutforce.html', attempts=attempts[::-1])

    @login_required
    def post(self):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        if user.role != 'admin':
            return redirect('/error/right')

        all_attempts = Attempt.get_all()
        now = datetime.datetime.now()

        if 'find-team' in request.form:
            info_about_team = request.form['team']
            if len(info_about_team) == 1 and info_about_team != '0':
                if db.session.query(Team).filter(Team.id == int(info_about_team)).first():
                    team = db.session.query(Team).filter(Team.id == int(info_about_team)).first()
                else:
                    return render_template('errors/brutforce.html',
                                           error_text='Такой команды нет, мы везде посмотрели.', attempts=all_attempts[::-1])

            elif len(info_about_team) > 1:
                if db.session.query(Team).filter(Team.name == info_about_team).first():
                    team = db.session.query(Team).filter(Team.name == info_about_team).first()
                else:
                    return render_template('errors/brutforce.html',
                                           error_text='Такой команды нет, мы везде посмотрели.', attempts=all_attempts[::-1])
            elif info_about_team == '0':
                return render_template('errors/brutforce.html', error_text='Бро, иди поспи. Что-то не так.', attempts=all_attempts[::-1])

            normal_attempts = Attempt.get_all_attempt_from_team_name(team.name)
            labels = list()
            for i in normal_attempts:
                if str(now.day) == i.time[6:8]:
                    if i.time[:1] == '0':
                        labels.append(i.time[1:2])
                    labels.append(i.time[:2])

            numbers = list()
            for i in range(24):
                numbers.append(labels.count(str(i)))

            return render_template('errors/brutforce.html', attempts=normal_attempts, numbers_team=numbers,
                                   success_text='Уху! Сейчас посмотрим, как они там все порешали.', graphic_team=True)

        if 'find-room' in request.form:
            info_about_room = request.form['room-id']
            if not db.session.query(Room).filter(Room.id == int(info_about_room)).first():
                return render_template('errors/brutforce.html', attempts=all_attempts[::-1],
                                       error_text2='Нееее, чет ты напутал.')

            attempts = Attempt.get_all_attempt_in_room(int(info_about_room))

            labels = list()
            for i in attempts:
                if str(now.day) == i.time[6:8]:
                    if i.time[:1] == '0':
                        labels.append(i.time[1:2])
                    labels.append(i.time[:2])

            numbers = list()
            for i in range(24):
                numbers.append(labels.count(str(i)))
            return render_template('errors/brutforce.html', attempts=attempts, numbers_room=numbers,
                                   success_text2='Ну все, теперь точно всех вычислим.', graphic_room=True)
