from flask import render_template, redirect
from flask.views import MethodView
from flask_login import current_user, login_required


class ErrorStartTournamentController(MethodView):
    # Админ начинает соревнования, но есть еще не законченные
    @login_required
    def get(self):
        if current_user.role != 'admin':
            return redirect('/error/right')
        return render_template('errors/error_start_tournament.html')


class ErrorStopTournamentController(MethodView):
    # Админ останавливает соревнования, но активных сореванований вообще-то нет
    @login_required
    def get(self):
        if current_user.role != 'admin':
            return redirect('/error/right')
        return render_template('errors/error_stop_tournament.html')


class ErrorTournamentController(MethodView):
    def get(self):
        # Ошибка прав, то есть кто может посещать страницу соревнований, пока они не идут
        return render_template('errors/error_tournament_right.html')
