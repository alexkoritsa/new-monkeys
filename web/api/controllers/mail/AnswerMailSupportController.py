from flask import render_template, redirect
from flask.views import MethodView
from web import db
from web.api.models.mail import Mail
from web.api.models.user import User
from web.api.forms.WriteSupportForm import WriteSupportForm
import datetime
from flask_login import login_required, current_user


class AnswerSupportController(MethodView):
    @login_required
    def get(self, id_mail):
        mail = db.session.query(Mail).filter(Mail.id == id_mail).first()
        user = db.session.query(User).filter(User.login == current_user.login).first()

        if user.role != 'admin':
            return redirect('/error/right')

        Mail.update_by_id(mail.id, 'is_read', True)
        form = WriteSupportForm()
        return render_template('answerSupport.html', mail=mail, form=form)

    @login_required
    def post(self, id_mail):
        form = WriteSupportForm()
        mail = db.session.query(Mail).filter(Mail.id == id_mail).first()
        user = db.session.query(User).filter(User.login == current_user.login).first()
        time = datetime.datetime.now().strftime("%H:%M %d.%m.%Y")

        if form.validate_on_submit():
            target_user = db.session.query(User).filter(User.login == mail.author).first()
            User.update_by_login(target_user.login, 'mails', target_user.mails + 1)

            answer = Mail(form.title.data, form.content.data, time, user.login, mail.author, 'SUPPORT ANSWER', '0')
            answer.save()
            return redirect('/show/support')
        return render_template('answerSupport.html', form=form, mail=mail)
