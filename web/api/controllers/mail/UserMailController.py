from flask import render_template, redirect, request
from web.api.models.mail import Mail
from flask.views import MethodView
from web import ALLOWED_EXTENSIONS
from web import db, app
from web.api.forms.UserMailForms import WriteMailForm
from werkzeug.utils import secure_filename
import random
import os
import datetime
from web.api.models.user import User
from flask_login import login_required, current_user


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


class ShowMailPageController(MethodView):
    @login_required
    def get(self):
        mail = Mail.get_all_mail_for_user(current_user.login)
        return render_template('mymail.html', mails=mail[::-1])


'''
ПОКАЗАТЬ ВСЕ ВХОДЯЩИЕ
'''


class ShowAllSentMailController(MethodView):
    @login_required
    def get(self):
        mail = Mail.get_all_sent_user_mail(current_user.login)
        return render_template('sentMails.html', mails=mail[::-1])


class ShowMyMailController(MethodView):
    @login_required
    def get(self, id_mail):
        mail = db.session.query(Mail).filter(Mail.id == id_mail).first()
        if mail.author != current_user.login:
            return redirect('/error/right')
        return render_template('mySentMail.html', mail=mail)


'''
ОТВЕТИТЬ НА ЛЮБОЕ ПИСЬМО 
'''


class AnswerMailPageController(MethodView):
    @login_required
    def get(self, id_mail):
        mail = db.session.query(Mail).filter(Mail.id == id_mail).first()
        if mail.target != current_user.login:
            return redirect('/error/right')

        form = WriteMailForm()
        Mail.update_by_id(mail.id, 'is_read', True)
        User.update_by_login(current_user.login, 'mails', current_user.mails - 1)
        return render_template('answerMail.html', form=form, mail=mail)

    @login_required
    def post(self, id_mail):
        form = WriteMailForm()
        mail = db.session.query(Mail).filter(Mail.id == id_mail).first()
        user = db.session.query(User).filter(User.login == current_user.login).first()
        time = datetime.datetime.now().strftime("%H:%M %d.%m.%Y")

        if form.validate_on_submit():
            file = request.files.get('file')
            filename = '0'
            if file is not None and allowed_file(file.filename):
                code = ''.join(
                    [random.choice(list('123456789qwertyuiopasdfghjkzxcvbnmQWERTYUOPASDFGHJKLZXCVBNM')) for x in
                     range(12)])
                filename = code + user.code + secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

            if mail.kind == 'SUPPORT ANSWER':
                mail = Mail(form.title.data, form.content.data, time, user.login, 'SUPPORT', 'SUPPORT', filename)
                mail.save()
                return redirect('/mail')

            target_user = db.session.query(User).filter(User.login == mail.author).first()
            User.update_by_login(target_user.login, 'mails', target_user.mails + 1)

            mail = Mail(form.title.data, form.content.data, time, user.login, mail.author, 'USER MAIL', filename)
            mail.save()
            return redirect('/mail')
        return render_template('answerMail.html', form=form, mail=mail)


'''
НАПИСАТЬ ПИСЬМО ДРУГОМУ ПОЛЬЗОВАТЕЛЮ
'''


class WriteUserMailController(MethodView):
    @login_required
    def get(self):
        form = WriteMailForm()
        users = User.get_all()
        return render_template('writeMail.html', form=form, users=users)

    @login_required
    def post(self):
        form = WriteMailForm()
        user = db.session.query(User).filter(User.login == current_user.login).first()
        time = datetime.datetime.now().strftime("%H:%M %d.%m.%Y")
        users = User.get_all()

        if form.validate_on_submit():
            file = request.files.get('file')
            target = request.form['select-target']
            filename = '0'
            if file is not None and allowed_file(file.filename):
                code = ''.join(
                    [random.choice(list('123456789qwertyuiopasdfghjkzxcvbnmQWERTYUOPASDFGHJKLZXCVBNM')) for x in
                     range(12)])
                filename = code + user.code + secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

            target_user = db.session.query(User).filter(User.login == target).first()
            User.update_by_login(target_user.login, 'mails', target_user.mails + 1)

            mail = Mail(form.title.data, form.content.data, time, user.login, target, 'USER MAIL', filename)
            mail.save()
            return render_template('writeMail.html', form=form, users=users,
                                   success_text='Письмо успешно отправлено. Адресат уже его получил.')
        return render_template('writeMail.html', form=form, users=users)
