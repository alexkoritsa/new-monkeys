from flask import redirect, make_response, request
from flask.views import MethodView
from web import db
from web.api.models.likeQuest import LikeQuest
from web.api.models.user import User
from web.api.models.room import Room
from web.api.models.quest import Quest
from flask_login import login_required, current_user


class LikeController(MethodView):
    @login_required
    def get(self):
        cookie = request.cookies.get('quest')
        if cookie is None:
            return redirect('/error/right')

        finish_room_code = cookie[4:34]
        room = db.session.query(Room).filter(Room.code == finish_room_code).first()
        quest = db.session.query(Quest).filter(Quest.id == room.quest_id).first()
        user = db.session.query(User).filter(User.login == current_user.login).first()

        if quest.creator_team_id == user.team_id:
            return redirect('/error/right')

        like = LikeQuest(user.team_id, quest.id, True)
        like.save()

        resp = make_response(redirect('/tournament'))
        resp.set_cookie('quest', '', expires=0)
        return resp


class DislikeController(MethodView):
    @login_required
    def get(self):
        cookie = request.cookies.get('quest')
        if cookie is None:
            return redirect('/error/right')

        finish_room_code = cookie[4:34]
        room = db.session.query(Room).filter(Room.code == finish_room_code).first()
        quest = db.session.query(Quest).filter(Quest.id == room.quest_id).first()
        user = db.session.query(User).filter(User.login == current_user.login).first()

        if quest.creator_team_id == user.team_id:
            return redirect('/error/right')

        like = LikeQuest(user.team_id, quest.id, False)
        like.save()

        resp = make_response(redirect('/tournament'))
        resp.set_cookie('quest', '', expires=0)
        return resp
