from flask import render_template, redirect, request
from flask.views import MethodView
from web import db
from web.api.models.quest import Quest
from web.api.models.user import User
from web.api.models.team import Team
from web.api.models.room import Room
from web.api.models.answer import Answer
from web.api.models.finishQuest import FinishQuest
from flask_login import login_required, current_user


def get_all_answers_in_finish_room(id_quest):
    if db.session.query(Room).filter(Room.quest_id == id_quest).filter(Room.is_finish == 'True').first():
        room = db.session.query(Room).filter(Room.quest_id == id_quest).filter(Room.is_finish == 'True').first()
        all_answers = Answer.get_variants_in_room(room.id)
        answers = [all_answers[0], all_answers[1]]
        return answers


class QuestPageController(MethodView):
    @login_required
    def get(self, id):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        if user.team_id == 0 and (user.role != 'admin' or user.role != 'leader'):
            return redirect('/error/right')

        quest = db.session.query(Quest).filter(Quest.id == id).first()
        if quest.creator_team_id != user.team_id and (user.role != 'admin' or user.role != 'leader'):
            return redirect('/error/right')

        team = db.session.query(Team).filter(Team.id == quest.creator_team_id).first()
        rooms = Room.get_how_many_rooms_in_quest(id)
        all_rooms = Room.get_all()
        all_answers = Answer.get_all()
        action = '/quest/' + str(id)
        finish = db.session.query(FinishQuest).filter(FinishQuest.id_quest == id).\
            filter(FinishQuest.id_team == user.team_id).first()

        if finish:
            score = finish.score
        else:
            score = 0

        all_quests = Quest.get_all()
        all_first_rooms = list()
        for q in all_quests:
            all_first_rooms.append(db.session.query(Room).filter(Room.quest_id == q.id).first())
        return render_template('quest.html', quest=quest, user=user, team=team, rooms=rooms, all_rooms=all_rooms,
                               all_answers=all_answers, action=action, all_first_rooms=all_first_rooms, score=score,
                               finish_answers=get_all_answers_in_finish_room(id))

    @login_required
    def post(self, id):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        if user.team_id == 0:
            return redirect('/error/right')

        quest = db.session.query(Quest).filter(Quest.id == id).first()
        if quest.creator_team_id != user.team_id:
            return redirect('/error/right')

        team = db.session.query(Team).filter(Team.id == quest.creator_team_id).first()
        rooms = Room.get_how_many_rooms_in_quest(id)
        all_rooms = Room.get_all()
        all_answers = Answer.get_all()
        action = '/quest/' + str(id)
        finish = db.session.query(FinishQuest).filter(FinishQuest.id_quest == id).\
            filter(FinishQuest.id_team == user.team_id).first()
        if finish:
            score = finish.score
        else:
            score = 0

        all_quests = Quest.get_all()
        all_first_rooms = list()
        for q in all_quests:
            all_first_rooms.append(db.session.query(Room).filter(Room.quest_id == q.id).first())

        if 'finish-variant' in request.form:
            id_answer = request.form['id-finish-answer']
            id_room = request.form['id-next-finish-room']
            next_finish_room = db.session.query(Room).filter(Room.id == id_room).first()
            if next_finish_room and next_finish_room.quest_id == id:
                Answer.update_by_id(id_answer, 'next_room_id', next_finish_room.id)
                Answer.update_by_id(id_answer, 'next_room_code', next_finish_room.code)
                return render_template('quest.html', quest=quest, user=user, team=team, rooms=rooms, all_rooms=all_rooms,
                                       all_answers=all_answers, action=action, next_finish_room=next_finish_room,
                                       success_text='Связь установлена. Поздравляем!', all_first_rooms=all_first_rooms,
                                       score=score, finish_answers=get_all_answers_in_finish_room(id))

        if request.method == 'POST':
            id_room = request.form['id-room']
            id_answer = request.form['id-answer']
            next_room = db.session.query(Room).filter(Room.id == id_room).first()

            if next_room and next_room.quest_id == id:
                Answer.update_by_id(id_answer, 'next_room_id', id_room)
                Answer.update_by_id(id_answer, 'next_room_code', next_room.code)
                return render_template('quest.html', quest=quest, user=user, team=team, rooms=rooms, all_rooms=all_rooms,
                                       all_answers=all_answers, action=action, next_room=next_room,
                                       success_text='Связь установлена. Поздравляем!', all_first_rooms=all_first_rooms,
                                       score=score, finish_answers=get_all_answers_in_finish_room(id))
            else:
                return render_template('quest.html', quest=quest, user=user, team=team, rooms=rooms, all_rooms=all_rooms,
                                       all_answers=all_answers, action=action, next_room=next_room,
                                       error_text='Такой комнаты в этом квесте нет. Попробуйте еще раз.', score=score,
                                       finish_answers=get_all_answers_in_finish_room(id))
        return render_template('quest.html', quest=quest, user=user, team=team, rooms=rooms, all_rooms=all_rooms,
                               all_answers=all_answers, action=action, all_first_rooms=all_first_rooms, score=score,
                               finish_answers=get_all_answers_in_finish_room(id))

