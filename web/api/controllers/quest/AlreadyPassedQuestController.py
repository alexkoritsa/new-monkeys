from flask import render_template
from flask.views import MethodView


class AlredyPassedQuestController(MethodView):
    def get(self):
        return render_template('passed_quest.html')
