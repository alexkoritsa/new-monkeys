from flask import render_template, redirect, request
from flask_login import current_user, login_required
from flask.views import MethodView
from web.api.models.quest import Quest


class MarkQuestController(MethodView):
    @login_required
    def get(self):
        if current_user.role != 'admin':
            return redirect('/error/right')
        return render_template('quest_mark.html', all_quests=Quest.get_all())

    @login_required
    def post(self):
        if 'make-changes' in request.form:
            rating_from_leader = request.form['rating-leader']
            id_quest_update = request.form['quest-id']
            if int(rating_from_leader) > 3:
                return render_template('quest_mark.html', error_text='Рейтинг не может быть больше трех.',
                                       all_quests=Quest.get_all())
            Quest.update_by_id(id_quest_update, 'rating_from_leader', rating_from_leader)
            return render_template('quest_mark.html', all_quests=Quest.get_all(),
                                   success_text='Рейтинг успешно изменен!')
        return render_template('quest_mark.html', all_quests=Quest.get_all())
