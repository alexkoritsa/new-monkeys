from flask import render_template, redirect, request
from flask.views import MethodView
from web.api.forms.CreateQuestForm import CreateQuestForm
from web.api.models.quest import Quest
from web.api.models.room import Room
from web.api.models.user import User
from web.api.models.team import Team
from web import db
from flask_login import current_user, login_required


class QuestsPageController(MethodView):
    @login_required
    def get(self):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        if user.team_id == 0:
            return redirect('/error/right')
        form = CreateQuestForm()
        all_quests = Quest.get_all()

        all_first_rooms = list()
        for quest in all_quests:
            all_first_rooms.append(db.session.query(Room).filter(Room.quest_id == quest.id).first())

        return render_template('quests.html', user=user, form=form, all_quests=all_quests,
                               all_first_rooms=all_first_rooms,
                               how_much_quest=Quest.get_how_many_quest_from_one_team(user.team_id))

    @login_required
    def post(self):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        team = db.session.query(Team).filter(Team.id == user.team_id).first()
        all_quests = Quest.get_all()
        all_first_rooms = list()
        for quest in all_quests:
            all_first_rooms.append(db.session.query(Room).filter(Room.quest_id == quest.id).first())
        if user.team_id == 0:
            return redirect('/error/right')

        form = CreateQuestForm()
        if form.submit_create.data and form.validate_on_submit():
            rating = request.form['select_rating']
            quest = Quest(form.name.data, rating, user.team_id, team.region)
            quest.save()
            return redirect('/quests')
        return render_template('quests.html', form=form, user=user, all_quests=all_quests, all_first_rooms=all_first_rooms)
