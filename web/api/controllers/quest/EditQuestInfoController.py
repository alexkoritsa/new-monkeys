from flask import render_template, request, redirect
from flask.views import MethodView
from web import db
from web.api.models.quest import Quest
from web.api.models.user import User
from web.api.forms.EditQuestForm import EditQuestTitleForm
from flask_login import login_required, current_user


class EditQuestTitleController(MethodView):
    @login_required
    def get(self, id_quest):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        quest = db.session.query(Quest).filter(Quest.id == id_quest).first()
        if quest.creator_team_id != user.team_id:
            return redirect('/error/right')

        action = '/quest/edit/title/' + str(id_quest)
        myQuest = '/quest/' + str(id_quest)
        form = EditQuestTitleForm()
        return render_template('editing/editQuestTitle.html', quest=quest, action=action, myQuest=myQuest, form=form)

    @login_required
    def post(self, id_quest):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        quest = db.session.query(Quest).filter(Quest.id == id_quest).first()
        if quest.creator_team_id != user.team_id:
            return redirect('/error/right')

        action = '/quest/edit/title/' + str(id_quest)
        myQuest = '/quest/' + str(id_quest)
        form = EditQuestTitleForm()

        if form.validate_on_submit():
            if form.title.data == quest.name:
                return render_template('editing/editQuestTitle.html', quest=quest, action=action, myQuest=myQuest, form=form,
                                       error_text='Новое название не может совпадать со старым')
            Quest.update_by_id(id_quest, 'name', form.title.data)
            return render_template('editing/editQuestTitle.html', quest=quest, action=action, myQuest=myQuest, form=form,
                                   success_text='Название квеста успешно изменено!')
        return render_template('editing/editQuestTitle.html', quest=quest, action=action, myQuest=myQuest, form=form)


class EditQuestRatingController(MethodView):
    @login_required
    def get(self, id_quest):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        quest = db.session.query(Quest).filter(Quest.id == id_quest).first()
        if quest.creator_team_id != user.team_id:
            return redirect('/error/right')

        action = '/quest/edit/rating/' + str(id_quest)
        myQuest = '/quest/' + str(id_quest)
        return render_template('editing/editQuestRating.html', quest=quest, action=action, myQuest=myQuest)

    @login_required
    def post(self, id_quest):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        quest = db.session.query(Quest).filter(Quest.id == id_quest).first()
        if quest.creator_team_id != user.team_id:
            return redirect('/error/right')

        action = '/quest/edit/rating/' + str(id_quest)
        myQuest = '/quest/' + str(id_quest)

        if request.method == 'POST':
            new_rating = request.form['select_rating']
            Quest.update_by_id(id_quest, 'rating', new_rating)
            return render_template('editing/editQuestRating.html', quest=quest, action=action, myQuest=myQuest,
                                   success_text='Рейтинг успешно изменен!')
        return render_template('editing/editQuestRating.html', quest=quest, action=action, myQuest=myQuest)
