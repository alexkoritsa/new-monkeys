from flask import render_template, request
from flask.views import MethodView
from web import db
from web.api.models.room import Room
from web.api.models.quest import Quest
from flask_login import login_required


class FinishQuestController(MethodView):
    @login_required
    def get(self):
        cookie = request.cookies.get('quest')
        finish_room_code = cookie[4:34]

        room = db.session.query(Room).filter(Room.code == finish_room_code).first()
        quest = db.session.query(Quest).filter(Quest.id == room.quest_id).first()

        return render_template('finish_quest.html', quest=quest)
