from flask import render_template, redirect
from web.api.models.finishQuest import FinishQuest
from flask.views import MethodView
from web import db
from web.api.models.team import Team
from flask_login import login_required, current_user


class ScoreboardController(MethodView):
    @login_required
    def get(self):
        if current_user.role != 'admin' or current_user.role == 'leader':
            if current_user.active_tournament == 'INACTIVE':
                return redirect('/error/tournament')

        # Сам скорборд, обычная таблица
        all_scores = list()
        teams = Team.get_all()
        for i in teams:
            final_score = FinishQuest.get_score_team(i.id)
            need_save = str(final_score) + '|' + i.name
            all_scores.append(need_save)

        new_list = list()
        final = list()
        for i in range(len(all_scores)):
            new_list = all_scores[i].split('|')
            final.append(new_list)

        final = sorted(final, reverse=True)
        firstTeam = db.session.query(Team).filter(Team.name == final[0][1]).first()
        secondTeam = db.session.query(Team).filter(Team.name == final[1][1]).first()
        thirdTeam = db.session.query(Team).filter(Team.name == final[2][1]).first()
        return render_template('scoreboard.html', final=final, firstTeam=firstTeam, secondTeam=secondTeam,
                               thirdTeam=thirdTeam)
