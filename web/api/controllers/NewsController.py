from flask import render_template, redirect
from flask.views import MethodView
from web import db
from web.api.models.user import User
from web.api.forms.AddNewsForm import AddNewsForm
from web.api.models.news import News
import datetime
from flask_login import login_required, current_user


class NewsController(MethodView):
    @login_required
    def get(self):
        form = AddNewsForm()
        user = db.session.query(User).filter(User.login == current_user.login).first()
        all_news = News.get_all_reverse()
        return render_template('news.html', user=user, form=form, all_news=all_news)

    @login_required
    def post(self):
        form = AddNewsForm()
        user = db.session.query(User).filter(User.login == current_user.login).first()
        time = datetime.datetime.now().strftime("%H:%M %d.%m.%Y")
        all_news = News.get_all_reverse()

        if form.validate_on_submit():
            news = News(form.title.data, form.content.data, time, current_user.login, 'USUAL-NEWS', 0)
            news.save()
            return redirect('/news')
        return render_template('news.html', user=user, form=form, all_news=all_news)
