from flask import render_template
from flask.views import MethodView
from flask_login import login_required, current_user
from web.api.models.tournament import Tournament
from web.api.models.team import Team
from web import db
import ast


class AboutEachTournamentController(MethodView):
    @login_required
    def get(self, id):
        tournament = db.session.query(Tournament).filter(Tournament.id == id).first()
        winners = ast.literal_eval(tournament.winner)
        likes = ast.literal_eval(tournament.best_quest)

        firstTeam = db.session.query(Team).filter(Team.name == winners[0][1]).first()
        secondTeam = db.session.query(Team).filter(Team.name == winners[1][1]).first()
        thirdTeam = db.session.query(Team).filter(Team.name == winners[2][1]).first()
        return render_template('each_tournament.html', tournament=tournament, winners=winners,
                               likes=likes, firstTeam=firstTeam, secondTeam=secondTeam, thirdTeam=thirdTeam)
