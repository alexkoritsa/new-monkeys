from flask import redirect
from flask_login import current_user, login_required
from flask.views import MethodView
from web.api.models.user import User
from web.api.models.tournament import Tournament
from web.api.models.team import Team
import datetime
from web import db
from web.api.models.finishQuest import FinishQuest
from web.api.models.likeQuest import LikeQuest
from web.api.models.quest import Quest
from web.api.models.news import News


class StartTournamentController(MethodView):
    @login_required
    def get(self):
        if current_user.role != 'admin':
            return redirect('/error/right')

        if not db.session.query(Tournament).filter(Tournament.status == 'ACTIVE').first():
            time = datetime.datetime.now().strftime("%H:%M %d.%m.%Y")
            tournament = Tournament('ACTIVE', time, len(Team.get_all()))
            tournament.save()
            User.make_all_users_active()
            title = 'Ура, соревнования #' + str(tournament.id) + ' начались!'
            news = News(title,
                        'Теперь каждый может начать проходить квесты и получать за них очки! Ждем Вас на сранице '
                        'соревнований. Кто знает, может быть именно Вы наш победитель?', time, current_user.login,
                        'TOURNAMENT-START', 0)
            news.save()
            return redirect('/tournament')
        else:
            return redirect('/error/start/tournament')


class StopTournamentController(MethodView):
    @login_required
    def get(self):
        if current_user.role != 'admin':
            return redirect('/error/right')

        if db.session.query(Tournament).filter(Tournament.status == 'ACTIVE').first():
            tournament = db.session.query(Tournament).filter(Tournament.status == 'ACTIVE').first()
            time = datetime.datetime.now().strftime("%H:%M %d.%m.%Y")
            Tournament.update_by_id(tournament.id, 'end', time)

            # Сам скорборд, обычная таблица
            all_scores = list()
            teams = Team.get_all()
            for i in teams:
                final_score = FinishQuest.get_score_team(i.id)
                need_save = str(final_score) + '|' + i.name
                all_scores.append(need_save)

            new_list = list()
            final = list()
            for i in range(len(all_scores)):
                new_list = all_scores[i].split('|')
                final.append(new_list)

            final = str(sorted(final, reverse=True))
            Tournament.update_by_id(tournament.id, 'winner', final)

            # Лайки
            quests = Quest.get_all()
            all = LikeQuest.get_all()
            all_likes = list()
            for i in quests:
                res = LikeQuest.get_all_likes_to_quest(i.id)
                need_save = str(res) + '|' + i.name
                all_likes.append(need_save)

            new_like_list = list()
            final_like = list()
            for i in range(len(all_likes)):
                new_like_list = all_likes[i].split('|')
                final_like.append(new_like_list)

            final_like = str(sorted(final_like, reverse=True))
            Tournament.update_by_id(tournament.id, 'best_quest', final_like)

            User.make_all_users_inactive()
            FinishQuest.clean_table_in_finish_tournament()
            LikeQuest.clean_table_in_finish_tournament()

            title = 'Финиш соревнований #' + str(tournament.id)
            news = News(title, 'Теперь Вы можете посмотреть подробную информацию о прощедших соревнованиях и '
                               'поздравить победителя и посмотреть, какой квест был самый-самый крутой по мнению '
                               'участников!',
                        time, current_user.login, 'TOURNAMENT-FINISH', tournament.id)
            news.save()

            Tournament.update_by_id(tournament.id, 'status', 'FINISH')
            return redirect('/account')
        return redirect('/error/finish/tournament')
