from flask.views import MethodView
from flask import render_template, redirect, url_for
from web.api.models.user import User
from web.api.forms.RegForm import RegForm
from hashlib import sha256
import random
from web import db
from flask_login import login_user


class RegController(MethodView):
    def get(self):
        form = RegForm()
        return render_template('reg.html', form=form)

    def post(self):
        form = RegForm()
        if form.validate_on_submit():
            same_login = User.query.filter(User.login == form.login.data).count()
            same_email = User.query.filter(User.email == form.email.data).count()
            if same_login:
                return render_template('reg.html', form=form, error_text='Пользователь с таким логином уже существует.')
            if same_email:
                return render_template('reg.html', form=form, error_text='Пользователь с такой почтой уже существует.')

            code = ''.join([random.choice(list('123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM')) for x in range(8)])
            # secret_link = 'http://localhost:3000/confirm?code=' + code
            # msg = Message('Подтверждение', sender="alexkoritsa@yandex.ru", recipients=[form.email.data])
            # msg.body = 'Для подтверждения вашей учетной записи пройдите по этой ссылке: ' + secret_link
            # mail.send(msg)
            secret_pass = sha256(form.password.data.encode()).hexdigest()
            if form.login.data == 'koritsa':
                user_admin = User(form.name.data, form.login.data, form.email.data, secret_pass, code, 'admin')
                user_admin.save()
                login_user(user_admin)
                return redirect('/')
            user = User(form.name.data, form.login.data, form.email.data, secret_pass, code, 'user')
            user.save()
            login_user(user)
            return redirect(url_for('api.MainPage'))
        return render_template('reg.html', form=form)
