from flask import render_template, session
from flask.views import MethodView
from flask_mail import Message
import random
from web import db
from web import mail
from web.api.forms.ResetPasswordForm import ResetPasswordForm
from web.api.models.user import User


class ResetPasswordController(MethodView):
    def get(self):
        form = ResetPasswordForm()
        return render_template('reset.html', form=form)

    def post(self):
        form = ResetPasswordForm()
        if form.validate_on_submit():
            user_reset = User.query.filter(User.email == form.email.data).first()
            if user_reset:
                random_password = (''.join([random.choice(list('123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM'))
                         for x in range(8)]))
                user_reset.change_password_by_email(form.email.data, random_password)
                msg = Message('Восстановление пароля', sender="tdevelopment@yandex.ru", recipients=[form.email.data])
                msg.body = 'Новый пароль: ' + random_password
                mail.send(msg)
                return render_template('reset.html', form=form, success_text='Новый пароль выслан на Вашу почту!')
            return render_template('reset.html', form=form, error_text='Пользователя с такой почтой не существует.')
        return render_template('reset.html', form=form)
