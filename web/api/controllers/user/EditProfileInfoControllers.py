from flask import render_template, redirect, request
from flask.views import MethodView
from hashlib import sha256
from web import db
from web.api.forms.EditProfileInfoForms import EditNameForm, EditEmailForm, EditPasswordForm
from web.api.forms.AvatarForm import AvatarForm
from web.api.models.user import User
import random
from web import ALLOWED_EXTENSIONS, app
import os
from werkzeug.utils import secure_filename
from flask_login import login_required, current_user


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


class EditNameController(MethodView):
    @login_required
    def get(self):
        form = EditNameForm()
        user = db.session.query(User).filter(User.login == current_user.login).first()
        return render_template('editing/editUserName.html', form=form, user=user)

    @login_required
    def post(self):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        form = EditNameForm()
        if form.validate_on_submit():
            User.update_by_login(current_user.login, 'name', form.name.data)
            return render_template('editing/editUserName.html', form=form, success_text='Ваше имя успешно изменено!', user=user)
        return render_template('editing/editUserName.html', form=form, user=user)


class EditEmailController(MethodView):
    @login_required
    def get(self):
        form = EditEmailForm()
        user = db.session.query(User).filter(User.login == current_user.login).first()
        return render_template('editing/editUserEmail.html', form=form, user=user)

    @login_required
    def post(self):
        form = EditEmailForm()
        user = db.session.query(User).filter(User.login == current_user.login).first()

        if form.validate_on_submit():
            password_form = sha256(form.password.data.encode()).hexdigest()
            user_count = User.query.filter(User.login == current_user.login).filter(User.password == password_form)
            if user_count.count():
                User.update_by_login(current_user.login, 'email', form.email.data)
                return render_template('editing/editUserEmail.html', form=form, success_text='Ваша почта успешно изменена!', user=user)
            # Проверка на уникальность почты
            return render_template('editing/editUserEmail.html', form=form, error_text='Вы ввели неправильный пароль.', user=user)
        return render_template('editing/editUserEmail.html', form=form, user=user)


class EditPasswordController(MethodView):
    @login_required
    def get(self):
        form = EditPasswordForm()
        user = db.session.query(User).filter(User.login == current_user.login).first()
        return render_template('editing/editUserPassword.html', form=form, user=user)

    @login_required
    def post(self):
        form = EditPasswordForm()
        user = db.session.query(User).filter(User.login == current_user.login).first()

        if form.validate_on_submit():
            password_form = sha256(form.old_password.data.encode()).hexdigest()
            user_count = User.query.filter(User.login == current_user.login).filter(User.password == password_form)
            if user_count.count():
                secret_pass = sha256(form.new_password.data.encode()).hexdigest()
                User.update_by_login(current_user.login, 'password', secret_pass)
                return render_template('editing/editUserPassword.html', form=form, success_text='Пароль успешно изменен!', user=user)
            return render_template('editing/editUserPassword.html', form=form, error_text='Вы ввели неправильный старый пароль.', user=user)
        return render_template('editing/editUserPassword.html', form=form, user=user)


class EditAvatarController(MethodView):
    @login_required
    def get(self):
        form = AvatarForm()
        user = db.session.query(User).filter(User.login == current_user.login).first()
        avatar = 'photos/uploads/' + user.photo
        return render_template('editing/editUserAvatar.html', form=form, avatar=avatar, photo=user.photo, user=user)

    @login_required
    def post(self):
        form = AvatarForm()
        user = db.session.query(User).filter(User.login == current_user.login).first()
        avatar = 'photos/uploads/' + user.photo

        if form.submit_photo.data and form.validate_on_submit():
            file = request.files['file']
            if file and allowed_file(file.filename):
                code = ''.join([random.choice(list('123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM')) for x in range(8)])
                filename = code + user.code + secure_filename(file.filename)
                if User.check_photo_file_name(filename):
                    file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                else:
                    return redirect('/error/rights')

                User.update_by_login(current_user.login, 'photo', filename)
                return redirect('/account')
            return render_template('editing/editUserAvatar.html', form=form, avatar=avatar, error_text='Что-то пошло не так. Попробуйте еще раз.', photo=user.photo, user=user)
        return render_template('editing/editUserAvatar.html', form=form, avatar=avatar, photo=user.photo, user=user)
