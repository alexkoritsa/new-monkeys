from flask import redirect, render_template
from flask.views import MethodView
from web.api.forms.LoginForm import LoginForm
from web.api.models.user import User
from web.api.models.log import Log
from hashlib import sha256
from web import db
import datetime
from flask_login import login_user


class LoginController(MethodView):
    def get(self):
        form = LoginForm()
        return render_template('login.html', form=form)

    def post(self):
        form = LoginForm()
        time = datetime.datetime.now().strftime("%H:%M %d.%m.%Y")

        if form.validate_on_submit():
            password = sha256(form.password.data.encode()).hexdigest()
            user_count = User.query.filter(User.login == form.login.data).filter(User.password == password)
            if user_count.count():
                user_role = db.session.query(User).filter(User.login == form.login.data).first()
                if user_role.role == 'ban':
                    log = Log('BANNED LOG USER', '/login', time, form.login.data)
                    log.save()
                    return render_template('login.html', form=form, error_text='Вы забанены администраором. По всем вопросам обращайтесь в поддержку сайта.')
                user = db.session.query(User).filter(User.login == form.login.data).first()
                login_user(user)
                return redirect('/')
            log = Log('WRONG PASS USER', '/login', time, form.login.data)
            log.save()
            return render_template('login.html', form=form, error_text='Неправильный логин или пароль.')
        return render_template('login.html', form=form)
