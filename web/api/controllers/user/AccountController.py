from flask.views import MethodView
from flask import render_template, redirect, request, url_for
from web.api.models.user import User
from web.api.models.team import Team
from web.api.models.quest import Quest
from web.api.models.finishQuest import FinishQuest
from web.api.forms.AddTeamForm import AddTeamForm
from web.api.forms.AvatarForm import AvatarForm
from web import app, db, ALLOWED_EXTENSIONS
from werkzeug.utils import secure_filename
import random
import os
from flask_login import current_user, login_required
from web.api.models.region import Region


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def get_region(region):
    areas = {
        '1': 'Математики',
        '2': 'Физики и химики',
        '3': 'Астрономы',
        '4': 'Спортсмены',
        '5': 'Игроки',
        '6': 'Географы и историки',
        '7': 'Биологи и генетики',
        '8': 'Тролли',
        '9': 'Программисты',
        '10': 'Взломщики',
        '11': 'Оккультисты',
        '12': 'Творцы'
    }
    return areas[region]


class AccountController(MethodView):
    @login_required
    def get(self):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        team = db.session.query(Team).filter(Team.id == user.team_id).first()
        all_quests = Quest.get_all()

        path_to_user_avatar = 'photos/uploads/' + user.photo

        # Турнирная таблица
        all_scores = list()
        teams = Team.get_all()
        for i in teams:
            final_score = FinishQuest.get_score_team(i.id)
            need_save = str(final_score) + '|' + i.name
            all_scores.append(need_save)

        new_list = list()
        final = list()
        for i in range(len(all_scores)):
            new_list = all_scores[i].split('|')
            final.append(new_list)

        final = sorted(final, reverse=True)

        # Всякие нужные на фронтенде присваивания
        team_name = ''
        path_to_team_avatar = ''
        if user.team_id != 0:
            team_name = team.name
            path_to_team_avatar = 'photos/uploads/' + team.photo

        return render_template('account.html', all_users=User.get_all(), user=user, form=AddTeamForm(), form1=AvatarForm(),
                               avatar=path_to_user_avatar, myTeam=team_name, team=team, team_avatar=path_to_team_avatar,
                               all_quests=all_quests, final=final)

    @login_required
    def post(self):
        form = AddTeamForm()
        form1 = AvatarForm()
        login = current_user.login
        user = db.session.query(User).filter(User.login == login).first()
        team = db.session.query(Team).filter(Team.id == user.team_id).first()
        myTeam = ''
        all_users = User.get_all()
        all_quests = Quest.get_all()
        avatar = 'photos/uploads/' + user.photo

        all_scores = list()
        teams = Team.get_all()
        for i in teams:
            final_score = FinishQuest.get_score_team(i.id)
            need_save = str(final_score) + '|' + i.name
            all_scores.append(need_save)

        new_list = list()
        final = list()
        for i in range(len(all_scores)):
            new_list = all_scores[i].split('|')
            final.append(new_list)

        final = sorted(final, reverse=True)

        if user.team_id != 0:
            myTeam = team.name
            team_avatar = 'photos/uploads/' + team.photo

        if form.validate_on_submit() and form.submit_team.data:
            same_name = Team.query.filter(Team.name == form.name.data).count()
            if same_name:
                return render_template('account.html', form=form, all_users=all_users, user=user,
                                       error_text='Команда с таким названием уже существует.', form1=form1,
                                       avatar=avatar, myTeam=myTeam, team=team, team_avatar=team_avatar,
                                       all_quests=all_quests, final=final)

            region_form = request.form['select_region']
            region = get_region(region_form)

            code = ''.join(
                [random.choice(list('123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM')) for x in
                 range(8)])
            team_new = Team(form.name.data, region, code, current_user.login)
            team_new.save()
            return redirect('/teams')

        if form1.submit_photo.data and form1.validate_on_submit():
            file = request.files['file']
            if file and allowed_file(file.filename):
                code = ''.join(
                    [random.choice(list('123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM')) for x in
                     range(8)])
                filename = code + user.code + secure_filename(file.filename)
                if User.check_photo_file_name(filename):
                    file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                else:
                    return redirect('/error/rights')
                User.update_by_login(login, 'photo', filename)
                return redirect('/account')
            return render_template('account.html', form=form, form1=form1, user=user, all_users=all_users,
                                   avatar=avatar, error_text_photo='Что-то пошло не так. Попробуйте еще раз.',
                                   myTeam=myTeam, team=team, team_avatar=team_avatar, all_quests=all_quests,
                                   final=final)

        if 'make-changes' in request.form:
            rating_from_leader = request.form['rating-leader']
            id_quest_update = request.form['quest-id']
            if int(rating_from_leader) > 3:
                return render_template('account.html', all_users=all_users, user=user, form=form, form1=form1, avatar=avatar,
                                       myTeam=myTeam, team=team, team_avatar=team_avatar, all_quests=all_quests,
                                       error_text='Рейтинг не может быть больше трех.', final=final)
            Quest.update_by_id(id_quest_update, 'rating_from_leader', rating_from_leader)
            return render_template('account.html', all_users=all_users, user=user, form=form, form1=form1, avatar=avatar,
                                   myTeam=myTeam, team=team, team_avatar=team_avatar, all_quests=all_quests,
                                   success_text2='Рейтинг успешно изменен!', final=final)
        return render_template('account.html', all_users=all_users, user=user, form=form, form1=form1, avatar=avatar,
                               myTeam=myTeam, team=team, team_avatar=team_avatar, all_quests=all_quests, final=final)
