from flask.views import MethodView
from flask import render_template, redirect
from web.api.models.user import User
from web import db
from flask_login import login_required, current_user


class GetAllUsersController(MethodView):
    @login_required
    def get(self):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        if user.role == 'user' or user.role == 'ban':
            return redirect('/error/right')

        all_users = User.get_all()
        return render_template('users.html', all_users=all_users)
