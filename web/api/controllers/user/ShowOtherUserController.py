from flask import render_template, redirect
from flask.views import MethodView
from web.api.models.user import User
from web.api.models.team import Team
from web import db
from flask_login import current_user, login_required


class ShowOtherUserController(MethodView):
    @login_required
    def get(self, login):
        if login == current_user.login:
            return redirect('/account')

        user = db.session.query(User).filter(User.login == login).first()
        avatar = 'photos/uploads/' + user.photo
        myTeam = ''

        if user.team_id != 0:
            myTeam = db.session.query(Team).filter(Team.id == user.team_id).first()
        return render_template('user.html', login=login, user=user, avatar=avatar, myTeam=myTeam)
