from flask import render_template
from flask.views import MethodView


class AboutUsPageController(MethodView):
    def get(self):
        return render_template('contact.html')
