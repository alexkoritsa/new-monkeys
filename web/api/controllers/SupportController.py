from flask import render_template, redirect, request
from web import db, app, ALLOWED_EXTENSIONS
from flask.views import MethodView
from web.api.forms.WriteSupportForm import WriteSupportForm
import datetime
from web.api.models.user import User
from web.api.models.mail import Mail
from werkzeug.utils import secure_filename
import random
import os
from flask_login import current_user, login_required


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


class ReadSupportController(MethodView):
    @login_required
    def get(self):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        if user.role != 'admin':
            return redirect('/error/right')
        mail = Mail.get_all_mail_for_admin()
        return render_template('errors/support.html', mail=mail)


class WriteSupportController(MethodView):
    @login_required
    def get(self):
        form = WriteSupportForm()
        users = User.get_all()
        return render_template('writeSupport.html', form=form, users=users)

    @login_required
    def post(self):
        form = WriteSupportForm()
        users = User.get_all()
        time = datetime.datetime.now().strftime("%H:%M %d.%m.%Y")
        user = db.session.query(User).filter(User.login == current_user.login).first()

        if form.validate_on_submit():
            file = request.files.get('file')
            filename = '0'
            if file is not None and allowed_file(file.filename):
                code = ''.join(
                    [random.choice(list('123456789qwertyuiopasdfghjkzxcvbnmQWERTYUOPASDFGHJKLZXCVBNM')) for x in
                     range(12)])
                filename = code + user.code + secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

            mail_for_support = Mail(form.title.data, form.content.data, time, user.login, 'SUPPORT', 'SUPPORT', filename)
            mail_for_support.save()
            return render_template('writeSupport.html', form=form, users=users,
                                   success_text='Письмо успешно отправлено. Мы уже изучаем Ваши правки!')
        return render_template('writeSupport.html', form=form, users=users)
