from flask import render_template, redirect
from flask.views import MethodView
from web import db
from web.api.models.team import Team
from web.api.models.user import User
from web.api.models.quest import Quest
from web.api.models.room import Room
from web.api.models.finishQuest import FinishQuest
from web.api.models.likeQuest import LikeQuest
from flask_login import login_required, current_user


class TournamentPageController(MethodView):
    @login_required
    def get(self):
        quests = Quest.get_all()
        user = db.session.query(User).filter(User.login == current_user.login).first()
        if user.role != 'admin' or user.role == 'leader':
            if current_user.active_tournament == 'INACTIVE':
                return redirect('/error/tournament')

        if user.team_id == 0:
            return redirect('/error/right')

        finish = FinishQuest.get_all()
        finish_empty = False
        if finish == []:
            finish_empty = True
        else:
            finish_empty = False
        team = db.session.query(Team).filter(Team.id == user.team_id).first()
        all_teams = Team.get_all()

        all_quests = Quest.get_all()
        all_first_rooms = list()
        for q in all_quests:
            all_first_rooms.append(db.session.query(Room).filter(Room.quest_id == q.id).first())

        # Скорборд
        all_scores = list()
        teams = Team.get_all()
        for i in teams:
            final_score = FinishQuest.get_score_team(i.id)
            need_save = str(final_score) + '|' + i.name
            all_scores.append(need_save)

        new_list = list()
        final = list()
        for i in range(len(all_scores)):
            new_list = all_scores[i].split('|')
            final.append(new_list)

        final = sorted(final, reverse=True)

        # Лайки
        all = LikeQuest.get_all()
        all_likes = list()
        for i in quests:
            res = LikeQuest.get_all_likes_to_quest(i.id)
            need_save = str(res) + '|' + i.name
            all_likes.append(need_save)

        new_like_list = list()
        final_like = list()
        for i in range(len(all_likes)):
            new_like_list = all_likes[i].split('|')
            final_like.append(new_like_list)

        final_like = sorted(final_like, reverse=True)

        return render_template('tournament.html', quests=quests, finish=finish, team=team, finish_empty=finish_empty,
                               user=user, all_teams=all_teams, all_first_rooms=all_first_rooms, final=final,
                               final_like=final_like)
