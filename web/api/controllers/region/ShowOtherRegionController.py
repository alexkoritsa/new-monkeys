from flask import render_template
from flask.views import MethodView
from flask_login import login_required
from web import db
from web.api.models.region import Region
from web.api.models.post import Post


class ShowOtherRegionController(MethodView):
    @login_required
    def get(self, id):
        region = db.session.query(Region).filter(Region.id == id).first()
        posts = Post.get_posts_in_region(region.name)
        return render_template('regions/show_other_region.html', region=region, posts=posts)
