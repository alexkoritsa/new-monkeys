from flask import render_template, redirect
from flask.views import MethodView
from web import db
from web.api.models.region import Region
from flask_login import login_required, current_user
from web.api.forms.EditRegionQuoteForm import EditRegionQuoteForm


class ChangeQuoteController(MethodView):
    @login_required
    def get(self, id):
        region = db.session.query(Region).filter(Region.id == id).first()
        if region.leader != current_user.login:
            return redirect('/error/right')

        form = EditRegionQuoteForm()
        return render_template('editing/editRegionQuote.html', form=form, region=region)

    @login_required
    def post(self, id):
        region = db.session.query(Region).filter(Region.id == id).first()
        if region.leader != current_user.login:
            return redirect('/error/right')

        form = EditRegionQuoteForm()
        if form.validate_on_submit():
            Region.update_by_name(region.name, 'quote', form.quote.data)
            Region.update_by_name(region.name, 'author_q', form.author.data)
            return render_template('editing/editRegionQuote.html', form=form, region=region,
                                   success_text='Ваш девиз успешно изменен!')
        return render_template('editing/editRegionQuote.html', form=form, region=region)
