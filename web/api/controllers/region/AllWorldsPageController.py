from flask import render_template, redirect
from flask.views import MethodView
from flask_login import login_required, current_user
from web.api.models.region import Region
from web.api.models.team import Team
from web import db


class AllWorldsPageController(MethodView):
    @login_required
    def get(self):
        if current_user.team_id == 0:
            return redirect('/error/right')
        regions = Region.get_all()
        team = db.session.query(Team).filter(Team.id == current_user.team_id).first()
        return render_template('regions/worlds.html', regions=regions, team=team)
