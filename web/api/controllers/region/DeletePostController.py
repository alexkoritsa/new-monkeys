from flask import redirect
from flask.views import MethodView
from web import db
from web.api.models.post import Post
from flask_login import login_required, current_user


class DeletePostController(MethodView):
    @login_required
    def get(self, id):
        post = db.session.query(Post).filter(Post.id == id).first()
        if current_user.role == 'user':
            return redirect('/error/right')

        if current_user.role == 'leader' and post.author != current_user.login:
            return redirect('/error/right')

        Post.query.filter(Post.id == id).delete()
        db.session.commit()
        return redirect('/my/world/edit')
