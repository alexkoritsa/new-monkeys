from web import db
from flask import render_template, redirect
from flask_login import current_user, login_required
from flask.views import MethodView
from web.api.models.team import Team
from web.api.models.region import Region
from web.api.models.post import Post


class ShowMyRegionController(MethodView):
    @login_required
    def get(self):
        if current_user.team_id == 0:
            return redirect('/error/right')
        region_team = db.session.query(Team).filter(Team.id == current_user.team_id).first()
        region = db.session.query(Region).filter(Region.name == region_team.region).first()
        posts = Post.get_posts_in_region(region.name)
        return render_template('regions/show_my_world.html', region=region, posts=posts[::-1])
