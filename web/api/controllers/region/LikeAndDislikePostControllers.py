from flask import redirect
from flask.views import MethodView
from flask_login import current_user, login_required
from web import db
from web.api.models.post import Post
from web.api.models.likePost import LikePost
from web.api.models.region import Region


class LikePostController(MethodView):
    @login_required
    def get(self, id_post, loop_index):
        post = db.session.query(Post).filter(Post.id == id_post).first()
        region = db.session.query(Region).filter(Region.name == post.region).first()
        if LikePost.check_is_post_liked(current_user.login, post.id):
            return redirect('/error/right')
        like = LikePost(current_user.login, post.id, True)
        like.save()
        return redirect('/post/' + str(region.id) + '/' + str(loop_index))


class DislikePostController(MethodView):
    @login_required
    def get(self, id_post, loop_index):
        post = db.session.query(Post).filter(Post.id == id_post).first()
        region = db.session.query(Region).filter(Region.name == post.region).first()
        if LikePost.check_is_post_liked(current_user.login, post.id):
            return redirect('/error/right')
        dislike = LikePost(current_user.login, post.id, False)
        dislike.save()
        return redirect('/post/' + str(region.id) + '/' + str(loop_index))
