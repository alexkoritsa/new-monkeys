from flask import render_template, redirect, request
from flask_login import login_required, current_user
from flask.views import MethodView
from web import db
from web.api.models.post import Post
import bleach


class ChangeStorylineController(MethodView):
    @login_required
    def get(self, id):
        post = db.session.query(Post).filter(Post.id == id).first()
        if current_user.login != post.author:
            return redirect('/error/right')

        return render_template('regions/edit_post.html', post=post)

    @login_required
    def post(self, id):
        post = db.session.query(Post).filter(Post.id == id).first()
        if current_user.login != post.author:
            return redirect('/error/right')

        if request.method == 'POST':
            cleaned_data = bleach.clean(request.form.get('editordata'), tags=bleach.sanitizer.ALLOWED_TAGS + ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'br', 'p', 'span', 'sup', 'sub', 'u', 'strike'], attributes=bleach.sanitizer.ALLOWED_STYLES + ['style'], styles=['text-align'])
            Post.update_by_id(post.id, 'html', cleaned_data)
            return redirect('/myworld')
        return render_template('regions/edit_post.html', post=post)
