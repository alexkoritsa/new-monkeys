from flask import render_template, redirect
from flask.views import MethodView
from web import db
from web.api.models.region import Region
from flask_login import login_required, current_user
from web.api.forms.EditRegionDescriptionForm import EditRegionDescriptionForm


class ChangeDescriptionController(MethodView):
    @login_required
    def get(self, id):
        region = db.session.query(Region).filter(Region.id == id).first()
        if region.leader != current_user.login:
            return redirect('/error/right')

        form = EditRegionDescriptionForm()
        return render_template('editing/editRegionDescription.html', form=form, region=region)

    @login_required
    def post(self, id):
        region = db.session.query(Region).filter(Region.id == id).first()
        if region.leader != current_user.login:
            return redirect('/error/right')

        form = EditRegionDescriptionForm()
        if form.validate_on_submit():
            Region.update_by_name(region.name, 'description', form.description.data)
            return render_template('editing/editRegionDescription.html', form=form, region=region,
                                   success_text='Описание Вашего края успешно изменено!')
        return render_template('editing/editRegionDescription.html', form=form, region=region)
