from flask import render_template, request, redirect
from flask_login import login_required, current_user
from flask.views import MethodView
from web import db, ALLOWED_EXTENSIONS, app
from web.api.models.region import Region
from web.api.models.team import Team
from web.api.models.post import Post
import bleach
import datetime
import random
import os
from werkzeug.utils import secure_filename


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


class EditMyRegionController(MethodView):
    @login_required
    def get(self):
        if current_user.team_id == 0 or current_user.role == 'user' or current_user.role == 'admin':
            return redirect('/error/right')

        region = db.session.query(Region).filter(Region.leader == current_user.login).first()
        posts = Post.get_all()
        return render_template('regions/edit_my_world.html', posts=posts[::-1], region=region)

    @login_required
    def post(self):
        team = db.session.query(Team).filter(Team.id == current_user.team_id).first()
        region = db.session.query(Region).filter(Region.leader == current_user.login).first()
        posts = Post.get_all()
        if request.method == 'POST':
            time = datetime.datetime.now().strftime("%H:%M %d.%m.%Y")
            cleaned_data = bleach.clean(request.form.get('editordata'), tags=bleach.sanitizer.ALLOWED_TAGS + ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'br', 'p', 'span', 'sup', 'sub', 'u', 'strike'], attributes=bleach.sanitizer.ALLOWED_STYLES + ['style'], styles=['text-align', 'color', 'background-color'])
            if cleaned_data == '':
                return render_template('regions/edit_my_world.html', error_text='Напишите что-нибудь!', posts=posts[::-1], region=region)

            file = request.files.get('file')
            filename = '0'

            if file is not None and allowed_file(file.filename):
                code = ''.join(
                    [random.choice(list('123456789qwertyuiopasdfghjkzxcvbnmQWERTYUOPASDFGHJKLZXCVBNM')) for x in
                     range(12)])
                filename = code + current_user.code + secure_filename(file.filename)
                if Post.check_photo_file_name(filename):
                    file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                else:
                    return redirect('/error/rights')

                post = Post(cleaned_data, team.region, current_user.login, time, filename)
                post.save()
                return redirect('/my/world/edit')

            post = Post(cleaned_data, team.region, current_user.login, time, filename)
            post.save()
            return redirect('/my/world/edit')
        return render_template('regions/edit_my_world.html', posts=posts[::-1], region=region)
