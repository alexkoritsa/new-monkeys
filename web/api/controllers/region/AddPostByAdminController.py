from flask import render_template, redirect, request
from flask.views import MethodView
from flask_login import current_user, login_required
from web import app, ALLOWED_EXTENSIONS
from web.api.models.post import Post
import datetime
import bleach
import random
import os
from werkzeug.utils import secure_filename


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def get_region(region):
    areas = {
        '1': 'Математики',
        '2': 'Физики и химики',
        '3': 'Астрономы',
        '4': 'Спортсмены',
        '5': 'Игроки',
        '6': 'Географы и историки',
        '7': 'Биологи и генетики',
        '8': 'Тролли',
        '9': 'Программисты',
        '10': 'Взломщики',
        '11': 'Оккультисты',
        '12': 'Творцы'
    }
    return areas[region]


class AddPostByAdminController(MethodView):
    @login_required
    def get(self):
        if current_user.role != 'admin':
            return redirect('/error/right')
        posts = Post.get_all()
        return render_template('regions/add_post_by_admin.html', posts=posts[::-1])

    @login_required
    def post(self):
        if current_user.role != 'admin':
            return redirect('/error/right')
        posts = Post.get_all()
        if request.method == 'POST':
            region = get_region(request.form.get('select_region'))
            time = datetime.datetime.now().strftime("%H:%M %d.%m.%Y")
            cleaned_data = bleach.clean(request.form.get('editordata'), tags=bleach.sanitizer.ALLOWED_TAGS + ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'br', 'p', 'span', 'sup', 'sub', 'u', 'strike'], attributes=bleach.sanitizer.ALLOWED_STYLES + ['style'], styles=['text-align', 'color', 'background-color'])
            if cleaned_data == '':
                return render_template('regions/edit_my_world.html', error_text='Напишите что-нибудь!', posts=posts[::-1], region=region)

            file = request.files.get('file')
            filename = '0'

            if file is not None and allowed_file(file.filename):
                code = ''.join(
                    [random.choice(list('123456789qwertyuiopasdfghjkzxcvbnmQWERTYUOPASDFGHJKLZXCVBNM')) for x in
                     range(12)])
                filename = code + current_user.code + secure_filename(file.filename)
                if Post.check_photo_file_name(filename):
                    file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                else:
                    return redirect('/error/rights')

                post = Post(cleaned_data, region, current_user.login, time, filename)
                post.save()
                return render_template('regions/add_post_by_admin.html', posts=posts[::-1],
                                       success_text='Пост успешно опубликован!')

            post = Post(cleaned_data, region, current_user.login, time, filename)
            post.save()
            return render_template('regions/add_post_by_admin.html', posts=posts[::-1], success_text='Пост успешно опубликован!')
        return render_template('regions/add_post_by_admin.html', posts=posts[::-1])
