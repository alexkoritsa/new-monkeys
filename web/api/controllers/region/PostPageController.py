from flask import render_template, redirect, request
from flask.views import MethodView
from flask_login import login_required, current_user
from web import db
from web.api.models.comments import Comment
from web.api.models.post import Post
from web.api.models.region import Region
from web.api.forms.AddCommentForm import AddCommentForm
import datetime
from web.api.models.likePost import LikePost
from web.api.models.log import Log


class PostPageController(MethodView):
    @login_required
    def get(self, id_region, loop_index):
        TIME = datetime.datetime.now().strftime("%H:%M %d.%m.%Y")
        region = db.session.query(Region).filter(Region.id == id_region).first()
        posts = Post.get_posts_in_region(region.name)
        try:
            post = posts[loop_index-1]
        except IndexError:
            log = Log('500', request.path, TIME, current_user.login)
            log.save()
            return render_template('errors/500.html')
        comments = Comment.get_comment_on_post(post.id)
        form = AddCommentForm()
        likes = LikePost.get_all_likes_to_post(post.id)
        isLike = LikePost.check_is_post_liked(current_user.login, post.id)

        which_like = ''
        if isLike:
            which_like = LikePost.get_what_choose_user(current_user.login, post.id)
        return render_template('regions/post_page.html', post=post, comments=comments[::-1], form=form, region=region,
                               loop_index=loop_index, likes=likes, which_like=which_like, isLike=isLike)

    @login_required
    def post(self, id_region, loop_index):
        TIME = datetime.datetime.now().strftime("%H:%M %d.%m.%Y")
        region = db.session.query(Region).filter(Region.id == id_region).first()
        posts = Post.get_posts_in_region(region.name)
        try:
            post = posts[loop_index-1]
        except IndexError:
            log = Log('500', request.path, TIME, current_user.login)
            log.save()
            return render_template('errors/500.html')
        comments = Comment.get_comment_on_post(post.id)
        form = AddCommentForm()
        isLike = LikePost.check_is_post_liked(current_user.login, post.id)
        which_like = ''
        if isLike:
            which_like = LikePost.get_what_choose_user(current_user.login, post.id)

        if form.validate_on_submit():
            time = datetime.datetime.now().strftime("%H:%M %d.%m.%Y")
            comment = Comment(form.content.data, current_user.login, time, post.id)
            comment.save()
            return redirect('/post/' + str(region.id) + '/' + str(post.id))
        return render_template('regions/post_page.html', post=post, comments=comments[::-1], form=form, region=region,
                               loop_index=loop_index, which_like=which_like, isLike=isLike)
