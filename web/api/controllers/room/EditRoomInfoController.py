from flask import render_template, request, redirect
from web.api.models.room import Room
from web.api.models.user import User
from web.api.forms.EditRoomForm import EditRoomTitleForm, EditRoomContentForm, EditRoomThingToEnter, EditRoomThingToUser
from web.api.models.quest import Quest
from flask.views import MethodView
from web.api.forms.AvatarForm import AvatarForm
from web import app, db, ALLOWED_EXTENSIONS
from werkzeug.utils import secure_filename
import random
import os
from flask_login import login_required, current_user


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


class EditTitleRoomController(MethodView):
    @login_required
    def get(self, id_quest, id_room):
        room = db.session.query(Room).filter(Room.id == id_room).first()
        user = db.session.query(User).filter(User.login == current_user.login).first()
        quest = db.session.query(Quest).filter(Quest.id == id_quest).first()
        if room.creator != user.team_id:
            return redirect('/error/right')

        action = '/quest/room/edit/title/' + str(id_quest) + '/' + str(id_room)
        myRoom = '/quest/showroom/' + str(id_quest) + '/' + str(id_room)
        form = EditRoomTitleForm()
        return render_template('editing/editRoomTitle.html', room=room, action=action, form=form, user=user, quest=quest, myRoom=myRoom)

    @login_required
    def post(self, id_quest, id_room):
        room = db.session.query(Room).filter(Room.id == id_room).first()
        user = db.session.query(User).filter(User.login == current_user.login).first()
        quest = db.session.query(Quest).filter(Quest.id == id_quest).first()
        if room.creator != user.team_id:
            return redirect('/error/right')

        action = '/quest/room/edit/title/' + str(id_quest) + '/' + str(id_room)
        myRoom = '/quest/showroom/' + str(id_quest) + '/' + str(id_room)
        form = EditRoomTitleForm()

        if form.validate_on_submit():
            if form.title.data == room.title:
                return render_template('editing/editRoomTitle.html', room=room, action=action, form=form, user=user,
                                       error_text='Новое название не может совпадать со старым.', quest=quest, myRoom=myRoom)

            Room.update_by_id(room.id, 'title', form.title.data)
            return render_template('editing/editRoomTitle.html', room=room, action=action, form=form, user=user,
                                   success_text='Название успешно изменено.', quest=quest, myRoom=myRoom)
        return render_template('editing/editRoomTitle.html', room=room, action=action, form=form, user=user, quest=quest, myRoom=myRoom)


class EditContentRoomController(MethodView):
    @login_required
    def get(self, id_quest, id_room):
        room = db.session.query(Room).filter(Room.id == id_room).first()
        user = db.session.query(User).filter(User.login == current_user.login).first()
        quest = db.session.query(Quest).filter(Quest.id == id_quest).first()
        if room.creator != user.team_id:
            return redirect('/error/right')

        action = '/quest/room/edit/content/' + str(id_quest) + '/' + str(id_room)
        myRoom = '/quest/showroom/' + str(id_quest) + '/' + str(id_room)
        form = EditRoomContentForm()
        return render_template('editing/editRoomContent.html', room=room, action=action, form=form, user=user, quest=quest, myRoom=myRoom)

    @login_required
    def post(self, id_quest, id_room):
        room = db.session.query(Room).filter(Room.id == id_room).first()
        user = db.session.query(User).filter(User.login == current_user.login).first()
        quest = db.session.query(Quest).filter(Quest.id == id_quest).first()
        if room.creator != user.team_id:
            return redirect('/error/right')

        action = '/quest/room/edit/content/' + str(id_quest) + '/' + str(id_room)
        myRoom = '/quest/showroom/' + str(id_quest) + '/' + str(id_room)
        form = EditRoomContentForm()

        if form.validate_on_submit():
            if form.content.data == room.question:
                return render_template('editing/editRoomContent.html', room=room, action=action, form=form, user=user,
                                       error_text='Новое условие не может совпадать со старым.', quest=quest, myRoom=myRoom)
            Room.update_by_id(id_room, 'question', form.content.data)
            return render_template('editing/editRoomContent.html', room=room, action=action, form=form, user=user,
                                   success_text='Название успешно изменено.', quest=quest, myRoom=myRoom)
        return render_template('editing/editRoomContent.html', room=room, action=action, form=form, user=user, quest=quest, myRoom=myRoom)


class EditThingsRoomController(MethodView):
    @login_required
    def get(self, id_quest, id_room):
        room = db.session.query(Room).filter(Room.id == id_room).first()
        user = db.session.query(User).filter(User.login == current_user.login).first()
        quest = db.session.query(Quest).filter(Quest.id == id_quest).first()
        if room.creator != user.team_id:
            return redirect('/error/right')

        action = '/quest/room/edit/things/' + str(id_quest) + '/' + str(id_room)
        myRoom = '/quest/showroom/' + str(id_quest) + '/' + str(id_room)
        form = EditRoomThingToUser()
        form1 = EditRoomThingToEnter()
        return render_template('editing/editRoomThings.html', room=room, action=action, form=form, user=user, quest=quest, myRoom=myRoom, form1=form1)

    @login_required
    def post(self, id_quest, id_room):
        room = db.session.query(Room).filter(Room.id == id_room).first()
        user = db.session.query(User).filter(User.login == current_user.login).first()
        quest = db.session.query(Quest).filter(Quest.id == id_quest).first()
        if room.creator != user.team_id:
            return redirect('/error/right')

        action = '/quest/room/edit/things/' + str(id_quest) + '/' + str(id_room)
        myRoom = '/quest/showroom/' + str(id_quest) + '/' + str(id_room)
        form = EditRoomThingToUser()
        form1 = EditRoomThingToEnter()

        if form.submit_to_user.data and form.validate_on_submit():
            if form.thing_to_user.data == room.thing_to_user:
                return render_template('editing/editRoomThings.html', room=room, action=action, form=form, user=user,
                                       quest=quest, myRoom=myRoom, form1=form1, error_text='Новый инвентарь не может совпадать со старым.')

            Room.update_by_id(id_room, 'thing_to_user', form.thing_to_user.data)
            return render_template('editing/editRoomThings.html', room=room, action=action, form=form, user=user,
                                   quest=quest, myRoom=myRoom, form1=form1, success_text='Инвентарь успешно изменен!')

        if form1.submit_to_enter.data and form1.validate_on_submit():
            if form1.thing_to_enter.data == room.thing_to_enter:
                return render_template('editing/editRoomThings.html', room=room, action=action, form=form, user=user,
                                       quest=quest, myRoom=myRoom, form1=form1, error_text='Новый инвентарь не может совпадать со старым.')

            Room.update_by_id(id_room, 'thing_to_enter', form1.thing_to_enter.data)
            return render_template('editing/editRoomThings.html', room=room, action=action, form=form, user=user,
                                   quest=quest, myRoom=myRoom, form1=form1, success_text='Инвентарь успешно изменен!')
        return render_template('editing/editRoomThings.html', room=room, action=action, form=form, user=user, quest=quest, myRoom=myRoom, form1=form1)


class EditPhotoRoomController(MethodView):
    @login_required
    def get(self, id_quest, id_room):
        room = db.session.query(Room).filter(Room.id == id_room).first()
        user = db.session.query(User).filter(User.login == current_user.login).first()
        quest = db.session.query(Quest).filter(Quest.id == id_quest).first()
        if room.creator != user.team_id:
            return redirect('/error/right')

        action = '/quest/room/edit/photo/' + str(id_quest) + '/' + str(id_room)
        myRoom = '/quest/showroom/' + str(id_quest) + '/' + str(id_room)
        form = AvatarForm()
        return render_template('editing/editRoomPhoto.html', room=room, action=action, form=form, user=user, quest=quest, myRoom=myRoom)

    @login_required
    def post(self, id_quest, id_room):
        room = db.session.query(Room).filter(Room.id == id_room).first()
        user = db.session.query(User).filter(User.login == current_user.login).first()
        quest = db.session.query(Quest).filter(Quest.id == id_quest).first()
        if room.creator != user.team_id:
            return redirect('/error/right')

        action = '/quest/room/edit/photo/' + str(id_quest) + '/' + str(id_room)
        myRoom = '/quest/showroom/' + str(id_quest) + '/' + str(id_room)
        form = AvatarForm()

        if form.submit_photo.data and form.validate_on_submit():
            file = request.files['file']
            if file and allowed_file(file.filename):
                code = ''.join(
                    [random.choice(list('123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM')) for x in
                     range(8)])
                filename = code + user.code + secure_filename(file.filename)
                if Room.check_photo_file_name(filename):
                    file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                else:
                    return redirect('/error/rights')

                Room.update_by_id(id_room, 'picture', filename)
                return render_template('editing/editRoomPhoto.html', room=room, action=action, form=form, user=user, quest=quest, myRoom=myRoom, success_text='Фото успешно изменено!')
            return render_template('editing/editRoomPhoto.html', room=room, action=action, form=form, user=user,
                                   quest=quest, myRoom=myRoom, error_text='Что-то пошло не так. Попробуйте еще раз!')
        return render_template('editing/editRoomPhoto.html', room=room, action=action, form=form, user=user, quest=quest, myRoom=myRoom)