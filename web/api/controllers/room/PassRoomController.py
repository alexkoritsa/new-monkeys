from flask import render_template, redirect, make_response
from flask.views import MethodView
from web.api.models.quest import Quest
from web.api.models.room import Room
from web.api.models.user import User
from web.api.models.answer import Answer
from web.api.models.teamToThing import TeamToThing
from web.api.models.finishQuest import FinishQuest
from web.api.models.attempt import Attempt
from web.api.models.team import Team
from web import db
from web.api.forms.InputAnswerForm import InputAnswerForm
import datetime
from flask_login import login_required, current_user


class PassRoomController(MethodView):
    @login_required
    def get(self, quest_id, room_code):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        quest = db.session.query(Quest).filter(Quest.id == quest_id).first()
        room = db.session.query(Room).filter(Room.code == room_code).first()
        answers = db.session.query(Answer).filter(Answer.room_id == room.id).all()
        things = db.session.query(TeamToThing).filter(TeamToThing.quest_id == quest_id).filter(TeamToThing.team_id
                                                                                               == user.team_id).all()
        thing_list = list()
        for i in range(len(things)):
            thing_list.append(things[i].thing)
        finish = db.session.query(FinishQuest).filter(FinishQuest.id_team == user.team_id).filter(FinishQuest.id_quest
                                                                                                  == quest_id).first()

        if finish and quest.creator_team_id != user.team_id:
            return redirect('/quest/already/passed')

        if room.quest_id != quest_id:
            return redirect('/error/right')

        if len(room.thing_to_enter) > 0:
            if not things:
                return redirect('/error/things')

            for i in range(len(things)):
                if thing_list.count(room.thing_to_enter) == 0:
                    return redirect('/error/things')
                else:
                    break

        if len(answers) > 1 and not db.session.query(TeamToThing).filter(TeamToThing.team_id == user.team_id).filter(TeamToThing.quest_id == quest_id).filter(TeamToThing.thing == room.thing_to_user).first() and not answers[0].content == 'Неправильный ответ':
            thing = TeamToThing(user.team_id, quest_id, room.thing_to_user)
            thing.save()

        answers_to_room = db.session.query(Answer).filter(Answer.room_id == room.id).all()
        if answers_to_room[0].content == 'Неправильный ответ':
            form = InputAnswerForm()
            action = '/quest/pass/' + str(quest_id) + '/' + str(room.id)
            return render_template('pass_answer.html', form=form, quest=quest, room=room, user=user, action=action,
                                   correct=False, isone=True, things=things)
        else:
            return render_template('pass_answer.html', quest=quest, room=room, user=user, answers=answers,
                                   isone=False, correct=False, things=things)

    @login_required
    def post(self, quest_id, room_code):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        room = db.session.query(Room).filter(Room.id == room_code).first()
        things = db.session.query(TeamToThing).filter(TeamToThing.quest_id == quest_id).filter(TeamToThing.team_id == user.team_id).all()
        thing_list = list()
        for i in range(len(things)):
            thing_list.append(things[i].thing)
        quest = db.session.query(Quest).filter(Quest.id == quest_id).first()
        team = db.session.query(Team).filter(Team.id == user.id).first()

        if room.quest_id != quest_id:
            return redirect('/error/right')

        if len(room.thing_to_enter) > 0:
            if not things:
                return redirect('/error/things')
            for i in range(len(things)):
                if thing_list.count(room.thing_to_enter) == 0:
                    return redirect('/error/things')
                else:
                    break

        answers_to_room = db.session.query(Answer).filter(Answer.room_id == room.id).all()
        time = datetime.datetime.now().strftime("%H:%M %d.%m.%Y")

        if answers_to_room[0].content == 'Неправильный ответ':
            form = InputAnswerForm()

            if form.validate_on_submit():
                # Проверка на правильность
                correct_answer = answers_to_room[1].content

                if form.answer.data == correct_answer:
                    if room.thing_to_user and not db.session.query(TeamToThing).filter(TeamToThing.team_id == user.team_id).filter(TeamToThing.quest_id == quest_id).filter(TeamToThing.thing == room.thing_to_user).first():
                        thing = TeamToThing(user.team_id, quest_id, room.thing_to_user)
                        thing.save()

                    if room.is_finish == 'True':
                        if quest.creator_team_id == user.team_id:
                            score = 0
                            end = FinishQuest(user.team_id, quest_id, score)
                            end.save()
                        else:
                            score = quest.rating_from_leader + quest.rating
                            end = FinishQuest(user.team_id, quest_id, score)
                            end.save()

                        attempt = Attempt(quest.id, room.id, team.name, 'QUEST', form.answer.data, time, True)
                        attempt.save()
                        resp = make_response(redirect('/quest/finish'))
                        resp.set_cookie('quest', user.code[:4] + room.code + user.code[4:])
                        return resp
                    else:
                        # Сохранение попытки сдачи правильного ответа
                        attempt = Attempt(quest.id, room.id, team.name, 'ROOM', form.answer.data, time, True)
                        attempt.save()

                    where = '/quest/pass/' + str(quest_id) + '/' + str(answers_to_room[1].next_room_code)
                    return redirect(where)

                elif form.answer.data != correct_answer:
                    if room.is_finish == 'True':
                        # Сохранение попытки сдачи неправильного ответа
                        attempt = Attempt(quest.id, room.id, team.name, 'QUEST', form.answer.data, time, False)
                        attempt.save()
                    else:
                        attempt = Attempt(quest.id, room.id, team.name, 'ROOM', form.answer.data, time, False)
                        attempt.save()

                    error = '/quest/pass/' + str(quest_id) + '/' + str(answers_to_room[0].next_room_code)
                    return redirect(error)
