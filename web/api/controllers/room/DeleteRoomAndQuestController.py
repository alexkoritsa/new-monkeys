from flask import redirect
from flask.views import MethodView
from web import db
from web.api.models.room import Room
from web.api.models.quest import Quest
from web.api.models.user import User
from web.api.models.answer import Answer
from flask_login import login_required, current_user


class DeleteRoomController(MethodView):
    @login_required
    def get(self, id_quest, id_room):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        quest = db.session.query(Quest).filter(Quest.id == id_quest).first()
        room = db.session.query(Room).filter(Room.id == id_room).first()

        if room.creator != user.team_id:
            return redirect('/error/right')

        if room.quest_id != quest.id:
            return redirect('/error/right')

        Room.query.filter(Room.id == id_room).delete()
        db.session.commit()

        Answer.query.filter(Answer.room_id == id_room).delete()
        db.session.commit()
        return redirect('/quest/' + str(id_quest))


class DeleteQuestController(MethodView):
    @login_required
    def get(self, id_quest):
        user = db.session.query(User).filter(User.login == current_user.login).first()
        quest = db.session.query(Quest).filter(Quest.id == id_quest).first()
        rooms = db.session.query(Room).filter(Room.quest_id == id_quest).all()

        if quest.creator_team_id != user.team_id:
            return redirect('/error/right')

        # for i in range(len(rooms)):
        #     Answer.query.filter(Room.id == rooms[i].id).delete()
        #     db.session.commit()

        Quest.query.filter(Quest.id == id_quest).delete()
        db.session.commit()
        # Room.query.filter(Room.quest_id == id_quest).delete()
        # db.session.commit()
        return redirect('/quests')
