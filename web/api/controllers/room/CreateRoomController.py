from flask.views import MethodView
from flask import render_template, redirect, request
from web.api.forms.CreateRoomForm import CreateRoomForm
from web.api.models.room import Room
from web.api.models.user import User
from web import app, db, ALLOWED_EXTENSIONS
from werkzeug.utils import secure_filename
import random
import os
from flask_login import login_required, current_user


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


class CreateRoomController(MethodView):
    @login_required
    def get(self, id):
        form = CreateRoomForm()
        action = '/quest/' + str(id) + '/create_room'
        return render_template('create_room.html', form=form, action=action)

    @login_required
    def post(self, id):
        form = CreateRoomForm()
        action = '/quest/' + str(id) + '/create_room'
        user = db.session.query(User).filter(User.login == current_user.login).first()

        if form.validate_on_submit():
            isFinish = request.form['select-finish']
            secret_code_for_room = ''.join([random.choice(list('123456789qwertyuiopasdfghjkzxcvbnmQWERTYUOPASDFGHJKLZXCVBNM')) for x in range(15)]) + ''.join([random.choice(list('123456789qwertyuiopasdfghjkzxcvbnmQWERTYUOPASDFGHJKLZXCVBNM')) for x in range(15)])
            file = request.files.get('file')
            filename = '0'
            if file is not None and allowed_file(file.filename):
                code = ''.join([random.choice(list('123456789qwertyuiopasdfghjkzxcvbnmQWERTYUOPASDFGHJKLZXCVBNM')) for x in range(12)])
                filename = code + user.code + secure_filename(file.filename)
                if Room.check_photo_file_name(filename):
                    file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                else:
                    return redirect('/error/rights')

            room = Room(form.title.data, form.question.data, filename, id, user.team_id, secret_code_for_room, isFinish,
                        form.thing_to_user.data, form.thing_to_enter.data)
            room.save()
            if room.add_answers(form.get_answers()):
                return redirect('/quest/' + str(id))
            else:
                form.errors['answers'] = 'Это поле обязательное'

        return render_template('create_room.html', form=form, action=action)
