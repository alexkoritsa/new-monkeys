from flask import render_template
from flask.views import MethodView
from web.api.models.quest import Quest
from web.api.models.team import Team
from web.api.models.user import User
from web import db
from flask_login import login_required


class ErrorNextRoomController(MethodView):
    @login_required
    def get(self, id_quest):
        quest = db.session.query(Quest).filter(Quest.id == id_quest).first()
        team = db.session.query(Team).filter(Team.id == quest.creator_team_id).first()
        user = db.session.query(User).filter(User.login == team.creator).first()
        return render_template('errors/error_next_room.html', user=user, quest=quest)
