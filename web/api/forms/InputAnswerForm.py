from flask_wtf import FlaskForm
from wtforms import SubmitField, TextAreaField
from wtforms.validators import DataRequired


class InputAnswerForm(FlaskForm):
    answer = TextAreaField('answer', validators=[DataRequired(message='Это обязательное поле.')])
    submit = SubmitField('submit')
