from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, SubmitField
from wtforms.validators import DataRequired


class EditRoomTitleForm(FlaskForm):
    title = StringField('title', validators=[DataRequired(message='Это обязательное поле.')])


class EditRoomContentForm(FlaskForm):
    content = TextAreaField('content', validators=[DataRequired(message='Это обязательное поле.')])


class EditRoomThingToUser(FlaskForm):
    thing_to_user = StringField('thing_to_user', validators=[DataRequired(message='Это обязательное поле.')])
    submit_to_user = SubmitField('submit_to_user')


class EditRoomThingToEnter(FlaskForm):
    thing_to_enter = StringField('thing_to_enter', validators=[DataRequired(message='Это обязательное поле.')])
    submit_to_enter = SubmitField('submit_to_enter')
