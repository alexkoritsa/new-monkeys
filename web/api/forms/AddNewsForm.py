from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, Length


class AddNewsForm(FlaskForm):
    title = StringField('title', validators=[DataRequired(message='Это поле обязательно.'),
                                             Length(max=120, message='Это поле не должно быть длинне 120 символов.')])
    content = TextAreaField('content', validators=[DataRequired(message='Это поле обязательно.')])
    submit = SubmitField()
