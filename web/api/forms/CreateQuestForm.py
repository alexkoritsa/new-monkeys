from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, RadioField
from wtforms.validators import DataRequired


class CreateQuestForm(FlaskForm):
    name = StringField('name', validators=[DataRequired(message='Это обязательное поле.')])
    submit_create = SubmitField('submit_create', validators=[DataRequired(message='Это обязательное поле.')])
