from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired, EqualTo, Length


class EditTeamPasswordForm(FlaskForm):
    password = StringField('password', validators=[DataRequired(message='Это обязательное поле'), Length(min=6, max=35)])
    confirm = PasswordField('conformpass', validators=[DataRequired(message='Это обязательное поле.'),
                                                     EqualTo('password', message='Пароли должны совпадать')])


class EditTeamNameForm(FlaskForm):
    name = StringField('name', validators=[DataRequired(message='Это обязательное поле.')])
