from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired, Length


class EditRegionQuoteForm(FlaskForm):
    quote = StringField('quote', validators=[DataRequired(message='Это обязательное поле.'), Length(max=175, min=25, message='Ваш девиз должен быть больше 25 символов и меньше 175.')])
    author = StringField('author', validators=[DataRequired(message='Это обязательно поле. '), Length(max=100, message='Имя автора должно быть не больше 100.')])
