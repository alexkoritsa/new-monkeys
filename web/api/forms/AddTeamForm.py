from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, Length


class AddTeamForm(FlaskForm):
    name = StringField('name', validators=[DataRequired(message='Это обязательное поле.'), Length(min=2, message='Название должно быть больше одного символа.')])
    submit_team = SubmitField('submit_team')
