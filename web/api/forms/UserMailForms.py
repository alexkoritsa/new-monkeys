from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField
from wtforms.validators import DataRequired, Length


class WriteMailForm(FlaskForm):
    title = StringField('title', validators=[DataRequired(message='Это обязательное поле.'),
                                             Length(max=25, message='Название не должно быть больше 25 символов.')])
    content = TextAreaField('content', validators=[DataRequired(message='Это обязательное поле.')])
