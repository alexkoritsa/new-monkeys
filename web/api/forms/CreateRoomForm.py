from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired
import json


class CreateRoomForm(FlaskForm):
    title = StringField(label='Название', validators=[DataRequired(message='Это обязательное поле.')])
    question = StringField(label='Вопрос', validators=[DataRequired(message='Это обязательное поле.')])
    answers = StringField('answers')
    thing_to_user = StringField('Получение инвентаря')
    thing_to_enter = StringField('Проверка уже имеющегося инвентаря')
    submit = SubmitField('Создать комнату')

    def get_answers(self):
        answers = list()
        if isinstance(self.answers.data, str):
            answers = json.loads(self.answers.data)
        elif isinstance(self.answers.data, list):
            answers = self.answers.data
        return answers
