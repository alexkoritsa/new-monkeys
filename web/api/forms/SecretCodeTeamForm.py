from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired


class SecretCodeTeamForm(FlaskForm):
    code = StringField('code', validators=[DataRequired(message='Это обязательное поле.')])


class SecretCodeTeamSubmitForm(FlaskForm):
    code = StringField('code', validators=[DataRequired(message='Это обязательное поле.')])
    submit = SubmitField('submit_photo')
