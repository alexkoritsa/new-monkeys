from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired, Email


class ResetPasswordForm(FlaskForm):
    email = StringField(label='Почта', validators=[DataRequired(message='Это обязательное поле.'),
                                                   Email(message='Некорректная электронная почта')])
