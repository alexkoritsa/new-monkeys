from flask_wtf import FlaskForm
from wtforms import SubmitField, TextAreaField
from wtforms.validators import DataRequired


class AddCommentForm(FlaskForm):
    content = TextAreaField('content', validators=[DataRequired(message='Это поле обязательно.')])
    submit = SubmitField()
