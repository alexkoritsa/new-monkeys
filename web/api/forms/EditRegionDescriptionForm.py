from flask_wtf import FlaskForm
from wtforms import TextAreaField
from wtforms.validators import DataRequired, Length


class EditRegionDescriptionForm(FlaskForm):
    description = TextAreaField('description', validators=[DataRequired(message='Это обязательное поле.'), Length(max=700, min=50, message='Ваше сообщение должно быть больше 50 символов и меньше 700.')])
