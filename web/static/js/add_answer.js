jQuery(function($){
    $('#add_answer').click(function() {
        var answerBox = $('#answers'),
            answerNumber = answerBox.children().length,
            answerTemplate = '<div class="form-row">' +
                    '<div class="form-group col-md-6 offset-md-3">' +
                         '<label>Вариант ответа №<span>' + (answerNumber + 1) + '</span></label>' +
                        '<input type="text" class="form-control answer_field">' +
                    '</div>' +
                '</div>';
        answerBox.append(answerTemplate);
    });

    $('#add_room_form').submit(function() {
        var answerFields = $('#answers').find('.answer_field'),
            answerValues = [];
        answerFields.each(function() {
            var ansValue = $(this).val();
            if (ansValue) {
                answerValues.push(ansValue);
            }
        });
        $('#add_room_form [name=answers]').val( JSON.stringify(answerValues) );
    });
});
