const message = `Любой дурак сможет написать код, который поймет машина. Хорошие программисты пишут код, который сможет понять человек.`;

const container = document.querySelector('#target');
let n;
function rerun(){
    container.textContent = '';
    n = 0;
    typist(message, container);
};
rerun();

function interval(letter){
    if(letter == ';' || letter == '.' || letter == ','){
        return Math.floor((Math.random() * 1) + 50);
    } else {
        return Math.floor((Math.random() * 1) + 50);
    }
}

function typist(text, target){
    if(typeof(text[n]) != 'undefined'){
        target.innerText += text[n];
    }
    n++;
    if(n < text.length){
        setTimeout(function(){
            typist(text, target)
        }, interval(text[n]));
    }
}