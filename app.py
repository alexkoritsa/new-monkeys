from web import app

if __name__ == '__main__':
    app.run(debug=True, port=10006, host='0.0.0.0')
