from web import db
from web.api.models.user import User
from web.api.models.team import Team
from web.api.models.quest import Quest
from web.api.models.userToTeam import UserToTeam
from random import randint
import random
from web.api.models.region import Region

db.drop_all()
db.create_all()

# Установки для начального запуска проекта
code = ''.join([random.choice(list('123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM')) for x in range(8)])

user = User('Александр Корицкий', 'koritsa', 'alexkoritsa@gmail.com',
            '1718c24b10aeb8099e3fc44960ab6949ab76a267352459f203ea1036bec382c2', code, 'admin')
user1 = User('Роман Чуйко', 'chuyko', 'test@gmail.com',
             '1718c24b10aeb8099e3fc44960ab6949ab76a267352459f203ea1036bec382c2', code, 'leader')
user.save()
user1.save()

team = Team('KoritsaTeam', 'Взломщики', '12341234', 'koritsa')
team.save()
userTo = UserToTeam(1, 1, True)
userTo.save()

team1 = Team('ChuykoTeam', 'Математики', '12341234', 'chuyko')
team1.save()
userTo1 = UserToTeam(2, 2, True)
userTo1.save()

User.update_by_login('koritsa', 'team_id', 1)
User.update_by_login('chuyko', 'team_id', 2)

# Удалить после окончания тестирования
quest = Quest('Первый квест', 1, 1, 'Взломщики')
quest.save()

for i in range(20):
    user = User('BOT' + str(i+1), 'bot' + str(i+1), str(i+1)+'@gmail.com',
                '1718c24b10aeb8099e3fc44960ab6949ab76a267352459f203ea1036bec382c2', code, 'user')
    user.save()

for i in range(5):
    team = Team('NewTeam' + str(i+1), 'Математики', '12341234', 'koritsa')
    team.save()

for i in range(20):
    id_team = randint(2,6)
    userToTeam = UserToTeam(randint(2, 11), id_team, True)
    userToTeam.save()
    User.update_by_login('bot' + str(i+1), 'team_id', id_team)

region1 = Region('Математики', 'Это край математиков!')
region2 = Region('Физики и химики', 'Это край физиков и химиков!')
region3 = Region('Астрономы', 'Это край астрономов!')
region4 = Region('Спортсмены', 'Это край спортсменов!')
region5 = Region('Игроки', 'Это край игроков!')
region6 = Region('Географы и историки', 'Это край географов и историков!')
region7 = Region('Биологи и генетики', 'Это край биологов и генетиков!')
region8 = Region('Тролли', 'Это край троллей!')
region9 = Region('Программисты', 'Это край программистов!')
region10 = Region('Взломщики', 'Это край взломщиков!')
region11 = Region('Оккультисты', 'Это край оккультистов!')
region12 = Region('Творцы', 'Это край творцов!')

region1.save()
region2.save()
region3.save()
region4.save()
region5.save()
region6.save()
region7.save()
region8.save()
region9.save()
region10.save()
region11.save()
region12.save()

db.session.commit()

Region.update_by_name('Взломщики', 'leader', 'koritsa')
Region.update_by_name('Математики', 'leader', 'chuyko')
