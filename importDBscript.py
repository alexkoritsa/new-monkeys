"""
WARNING! Поменяйте путь к бд или название, чтобы избежать ошибок.
Все работает исправно.
"""

import sqlite3

# Копируем данные. Необходима бд именно с пользователями
connect = sqlite3.connect('monkeys.sqlite')
mcur = connect.cursor()

mcur.execute("SELECT * FROM user")
users = mcur.fetchall()

mcur.execute("SELECT * FROM team")
teams = mcur.fetchall()

mcur.execute("SELECT * FROM user_to_team")
users_and_teams = mcur.fetchall()
connect.close()

# Переносим данные в нашу дб
conn = sqlite3.connect('database.sqlite')
cur = conn.cursor()

for user in users:
    cur.execute("INSERT OR REPLACE INTO user VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 'INACTIVE')", user)
    conn.commit()

for team in teams:
    cur.execute("INSERT OR REPLACE INTO team VALUES (?, ?, ?, ?, ?, ?)", team)
    conn.commit()

for i in users_and_teams:
    cur.execute("INSERT OR REPLACE INTO user_to_team VALUES (?, ?, ?, ?)", i)
    conn.commit()

conn.close()
